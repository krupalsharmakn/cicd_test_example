var bCrypt = require('bcrypt-nodejs');
var async = require('async');
var express = require('express');
var multiparty = require('multiparty');
var fs = require('fs');
var path = require('path');
var expressJwt = require('express-jwt');
var jwt = require('jsonwebtoken');
var email = require("../../utils/mailer");
var router = express.Router();
var BASE_API_URL = "";
var version = "1.0"; // version code
const admin = require('firebase-admin');
var app = require('../../app')
var configuration = require("../../config");

/* handler */
var MeetingRequestHandler = require("../../handlers/meeting_requests_handler");
var UserChangePasswordHandler = require("../../handlers/user_change_password_handler");
var UserForgotPasswordHandler = require("../../handlers/user_forgot_password_handler");
var ResetpassHandler = require("../../handlers/resetpasshandler");
var NewsHandler = require("../../handlers/news_handler");
var ComplaintHandler = require("../../handlers/complaint_handler");
var AchievementHandler = require("../../handlers/achievements_handler");
var MlaProfileHandler = require("../../handlers/mla_profile_handler");
var UserHandler = require("../../handlers/user_handler");
var EventHandler = require("../../handlers/event_handler")
var ProjectHandler = require("../../handlers/projects_handler");
var DepartmentHandler = require("../../handlers/department_handler");
var PartyworkerHandler = require("../../handlers/partyworker_handler");
var CountHandler = require("../../handlers/count_handler");
var createHash = function (password) {
    return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
};

var isAuthenticated = function (req, res, next) {
    if (req.isAuthenticated()) {
        next();
    }
    res.redirect(BASE_API_URL + '/');
};

var isAuthenticatedAccessToken = function (req, res, next) {
    var token = req.body.token || req.query.token || req.headers.authorization;
    if (token) {
        jwt.verify(token, configuration.TOKEN_SECRET, function (err, decoded) {
            if (err) {
                return res.json({
                    success: false,
                    message: 'Failed to authenticate token.'
                });
            } else {
                req.user = decoded;
                next();
            }
        });
    } else {
        return res.status(403).send({
            statuscode: 203,
            msgkey: "api.access.token.failed",
            v: version
        });
    }
}
module.exports = function (passport) {

    router.get(BASE_API_URL + '/', function (req, res) {
        var reponseJson = {
            statuscode: 200,
            msgkey: "login.first.to.access.api",
            v: version
        };
        res.json(reponseJson);
    });

    router.get(BASE_API_URL + '/_signupfailure', function (req, res) {
        var reponseJson = {
            statuscode: 203,
            msgkey: "auth.signup.exist",
            v: version
        };
        res.json(reponseJson);
    });

    router.post(BASE_API_URL + '/login', function (req, res, next) {
        passport.authenticate('login', function (err, user, info) {
            if (err) {
                var reponseJson = {
                    statuscode: 203,
                    msgkey: "login failure",
                    v: version
                };
                res.json(reponseJson);
            } else if (info) {
                var reponseJson = {
                    statuscode: 204,
                    msgkey: "login failure",
                    v: version
                };
                res.json(reponseJson);
            } else {
                var reponseJson = {
                    statuscode: 200,
                    msgkey: "login success",
                    v: version,
                    data: user
                };
                res.json(reponseJson);
            }
        })(req, res, next);
    });

    router.get(BASE_API_URL + '/home', function (req, res) {
        if (req.user.username) {
            var reponseJson = {
                statuscode: 200,
                msgkey: "login.success",
                v: version,
                data: req.user
            };
        }
        res.json(reponseJson);
    });

    router.get(BASE_API_URL + '/_loginfailure', function (req, res) {
        var reponseJson = {
            statuscode: 203,
            msgkey: "login.failure",
            registered: false,
            v: version
        };
        res.json(reponseJson);
    });

    router.get(BASE_API_URL + '/logout', function (req, res) {
        req.session.destroy(function (err) {
            var reponseJson = {
                statuscode: 200,
                msgkey: "logout.success",
                v: version
            };
            res.json(reponseJson);
        });

    });

    /* API for Handle Registration POST */
    router.post(BASE_API_URL + '/signup', function (req, res, next) {
        var db = app.db;
        const usersCollection = db.collection("users");
        passport.authenticate('signup', function (err, user, info) {
            if (err) {
                var reponseJson = {
                    statuscode: 203,
                    msgkey: "auth.signup.exist",
                    v: version
                };
                res.json(reponseJson);
            } else if (info) { } else {
                var registered = false;
                var reponseJson = {
                    statuscode: 200,
                    msgkey: "auth.signup.success",
                    username: user.username,
                    data: user,
                    v: version
                };
                req.logout();
                res.json(reponseJson);
            }
        })(req, res, next);
    });

    /* API for signup success page */
    router.get(BASE_API_URL + '/_signupsuccess', function (req, res) {
        var registered = false;
        if (req.user.isActive === "1") {
            registered = true;
        }

        var reponseJson = {
            statuscode: 200,
            msgkey: "auth.signup.success",
            registered: registered,
            username: req.user.username,
            v: version
        };
        req.logout();
        res.json(reponseJson);
    });

    router.post(BASE_API_URL + '/user_change_password', function (req, res) {
        var data = req.body;
        UserChangePasswordHandler.user_change_password(data, function (response) {
            response.version = version;
            res.json(response);
        });
    });

    router.post(BASE_API_URL + '/user_forgot_password', function (req, res) {
        var data = req.body;
        UserForgotPasswordHandler.user_forgot_password(data, function (response) {
            response.version = version;
            res.json(response);
        });
    });

    router.post(BASE_API_URL + '/resetpassword', function (req, res) {
        var newpassword = req.body.newpassword;
        var confirmpassword = req.body.confirmpassword;
        var resetpassword = req.body.resetpassword;
        var data = {
            "password": newpassword,
            "confirmpassword": confirmpassword,
            "resetpassword": resetpassword
        };
        ResetpassHandler.resetpassword_data(data, function (response) {
            response.version = version;
            res.json(response);
        });
    });

    router.post(BASE_API_URL + '/add_images', function (req, res) {
        var data = req.body;
        var eventpath = req.files;
        EventHandler.add_images(data, eventpath, function (response) {
            response.version = version;
            res.json(response);
        });
    });

    router.post(BASE_API_URL + '/new_meeting_request', isAuthenticatedAccessToken, function (req, res) {
        var data = req.body;
        MeetingRequestHandler.new_meeting_request(data, function (response) {
            response.version = version;
            res.json(response);
        });
    });

    router.post(BASE_API_URL + '/update_meeting_request', isAuthenticatedAccessToken, function (req, res) {
        var data = req.body;
        MeetingRequestHandler.update_meeting_request(data, function (response) {
            response.version = version;
            res.json(response);
        });
    });

    router.post(BASE_API_URL + '/deactivate_meeting_request', isAuthenticatedAccessToken, function (req, res) {
        var data = req.body;
        MeetingRequestHandler.deactivate_meeting_request(data, function (response) {
            response.version = version;
            res.json(response);
        });
    });

    router.get(BASE_API_URL + '/get_meeting_request', isAuthenticatedAccessToken, function (req, res) {
        var data = req.query;
        MeetingRequestHandler.get_meeting_request(data, function (response) {
            response.version = version;
            res.json(response);
        });
    });

    router.get(BASE_API_URL + '/get_user', isAuthenticatedAccessToken, function (req, res) {
        var data = req.query;
        UserHandler.get_user(data, function (response) {
            response.version = version;
            res.json(response);
        });
    });


    router.post(BASE_API_URL + '/user_signup_login', function (req, res) {
        var data = req.body;
        UserHandler.user_signup_login(data, function (response) {
            response.version = version;
            res.json(response);
        });

    });

    router.post(BASE_API_URL + '/check_otp_request', function (req, res) {
        var data = req.body;
        UserHandler.check_otp_request(data, function (response) {
            response.version = version;
            res.json(response);
        });

    });

    router.post(BASE_API_URL + '/edit_user_profile', isAuthenticatedAccessToken, function (req, res) {
        var data = req.body;
        var eventpath = req.files;
        UserHandler.edit_user_profile(data, eventpath, function (response) {
            response.version = version;
            res.json(response);
        });
    });

    router.post(BASE_API_URL + '/delete_user', isAuthenticatedAccessToken, function (req, res) {
        var data = req.body;
        UserHandler.delete_user(data, function (response) {
            response.version = version;
            res.json(response);
        });
    });


    router.post(BASE_API_URL + '/add_news', isAuthenticatedAccessToken, function (req, res) {
        var data = req.body;
        var eventpath = req.files;
        NewsHandler.add_news(data, eventpath, function (response) {
            response.version = version;
            res.json(response);
        });
    });

    router.post(BASE_API_URL + '/edit_news', isAuthenticatedAccessToken, function (req, res) {
        var data = req.body;
        var eventpath = req.files;
        NewsHandler.edit_news(data, eventpath, function (response) {
            response.version = version;
            res.json(response);
        });
    });

    router.post(BASE_API_URL + '/add_complaint', isAuthenticatedAccessToken, function (req, res) {
        var data = req.body;
        var eventpath = req.files;
        ComplaintHandler.add_complaint(data, eventpath, function (response) {
            response.version = version;
            res.json(response);
        });

    });

    router.post(BASE_API_URL + '/update_complaint_status', isAuthenticatedAccessToken, function (req, res) {
        var data = req.body;
        ComplaintHandler.update_complaint_status(data, function (response) {
            response.version = version;
            res.json(response);
        });

    });

    router.get(BASE_API_URL + '/list_complaints_user_id', isAuthenticatedAccessToken, function (req, res) {
        var data = req.query;
        ComplaintHandler.list_complaints_user_id(data, function (response) {
            response.version = version;
            res.json(response);
        });

    });

    router.get(BASE_API_URL + '/list_complaints_complaint_status', isAuthenticatedAccessToken, function (req, res) {
        var data = req.query;
        ComplaintHandler.list_complaints_complaint_status(data, function (response) {
            response.version = version;
            res.json(response);
        });

    });

    router.get(BASE_API_URL + '/complaint_category', function (req, res) {
        var response = {};
        fs.readFile('./taluk.json', 'utf8', (err, jsonString) => {
            if (err) {
                console.log("File read failed:", err)
                return
            }
            var jsondata = JSON.parse(jsonString)
            response.complaint_categories = jsondata;
            response.statuscode = 200;
            res.json(response);
        })

    });

    router.get(BASE_API_URL + '/filter_category', function (req, res) {
        var response = {};
        fs.readFile('./panchayath.json', 'utf8', (err, jsonString) => {
            if (err) {
                console.log("File read failed:", err)
                return
            }
            var jsondata = JSON.parse(jsonString)
            response.complaint_categories = jsondata;
            response.statuscode = 200;
            res.json(response);
        })

    });


    router.get(BASE_API_URL + '/banner_images', function (req, res) {
        var response = {};
        var imageFolder = "./images/banner_images";

        var image_list = [];
        fs.readdirSync(imageFolder).forEach(file => {
            newimage = configuration.get_banner_image + file
            image_list.push(newimage)
        });


        var list = [];
        id = 1;
        for (var i = 0; i < image_list.length; i++) {
            var objdata = {
                "image_id": id++,
                "image_url": image_list[i]
            }
            list.push(objdata)
        }
        response.statuscode = 200;
        response.complaint_categories = list;

        res.json(response);
    });

    router.post(BASE_API_URL + '/add_banner_images', function (req, res) {
        var eventpath = req.files;
        var data = req.body
        MlaProfileHandler.add_banner_images(data, eventpath, function (response) {
            response.version = version;
            res.json(response);
        });
    });


    router.post(BASE_API_URL + '/add_achievement', isAuthenticatedAccessToken, function (req, res) {
        var data = req.body;
        var eventpath = req.files;
        AchievementHandler.add_achievement(data, eventpath, function (response) {
            response.version = version;
            res.json(response);
        });

    });

    router.post(BASE_API_URL + '/edit_achievement', isAuthenticatedAccessToken, function (req, res) {
        var data = req.body;
        var eventpath = req.files;
        AchievementHandler.edit_achievement(data, eventpath, function (response) {
            response.version = version;
            res.json(response);
        });
    });

    router.post(BASE_API_URL + '/deactivate_achievement', isAuthenticatedAccessToken, function (req, res) {
        var data = req.body;
        AchievementHandler.deactivate_achievement(data, function (response) {
            response.version = version;
            res.json(response);
        });
    });

    router.post(BASE_API_URL + '/add_mla_profile', isAuthenticatedAccessToken, function (req, res) {
        var data = req.body;
        var eventpath = req.files;
        MlaProfileHandler.add_mla_profile(data, eventpath, function (response) {
            response.version = version;
            res.json(response);
        });
    });

    router.post(BASE_API_URL + '/edit_mla_profile', isAuthenticatedAccessToken, function (req, res) {
        var data = req.body;
        var eventpath = req.files;
        MlaProfileHandler.edit_mla_profile(data, eventpath, function (response) {
            response.version = version;
            res.json(response);
        });
    });

    router.get(BASE_API_URL + '/get_mla_profile', function (req, res) {
        var data = req.query;
        MlaProfileHandler.get_mla_profile(data, function (response) {
            response.version = version;
            res.json(response);
        });
    });


    router.get(BASE_API_URL + '/get_achievement', function (req, res) {
        var data = req.query;
        AchievementHandler.get_achievement(data, function (response) {
            response.version = version;
            res.json(response);
        });
    });

    router.get(BASE_API_URL + '/get_complaint', isAuthenticatedAccessToken, function (req, res) {
        var data = req.query;
        ComplaintHandler.get_complaint(data, function (response) {
            response.version = version;
            res.json(response);
        });
    });

    router.get(BASE_API_URL + '/get_news', function (req, res) {
        var data = req.query;
        NewsHandler.get_news(data, function (response) {
            response.version = version;
            res.json(response);
        });
    });

    router.get(BASE_API_URL + '/list_news_by_category', function (req, res) {
        var data = req.query;
        NewsHandler.list_news_by_category(data, function (response) {
            response.version = version;
            res.json(response);
        });
    });

    router.get(BASE_API_URL + '/list_news_images', function (req, res) {
        NewsHandler.list_news_images(function (response) {
            response.version = version;
            res.json(response);
        });
    });

    router.post(BASE_API_URL + '/deactivate_news', isAuthenticatedAccessToken, function (req, res) {
        var data = req.body;
        NewsHandler.deactivate_news(data, function (response) {
            response.version = version;
            res.json(response);
        });
    });


    //events
    router.post(BASE_API_URL + '/add_event', isAuthenticatedAccessToken, function (req, res) {
        var data = req.body;
        var eventpath = req.files;
        EventHandler.add_event(data, eventpath, function (response) {
            response.version = version;
            res.json(response);
        });
    });

    router.post(BASE_API_URL + '/events_filter', function (req, res) {
        var data = req.body;
        EventHandler.event_filter(data, function (response) {
            response.version = version;
            res.json(response);
        });
    });

    router.get(BASE_API_URL + '/get_event', isAuthenticatedAccessToken, function (req, res) {
        var data = req.query;
        console.log("inside index")
        EventHandler.get_event(data, function (response) {
            response.version = version;
            res.json(response);
        });
    });


    router.post(BASE_API_URL + '/deactivate_event', isAuthenticatedAccessToken, function (req, res) {
        var data = req.body;
        EventHandler.deactivate_event(data, function (response) {
            response.version = version;
            res.json(response);
        });
    });

    router.post(BASE_API_URL + '/edit_event', isAuthenticatedAccessToken, function (req, res) {
        var data = req.body;
        var eventpath = req.files;
        EventHandler.edit_event(data, eventpath, function (response) {
            response.version = version;
            res.json(response);
        });
    });

    router.post(BASE_API_URL + '/add_project', isAuthenticatedAccessToken, function (req, res) {
        var data = req.body;
        var eventpath = req.files;
        ProjectHandler.add_project(data, eventpath, function (response) {
            response.version = version;
            res.json(response);
        });
    });

    router.get(BASE_API_URL + '/get_project', function (req, res) {
        var data = req.query;
        ProjectHandler.get_project(data, function (response) {
            response.version = version;
            res.json(response);
        });
    });


    router.post(BASE_API_URL + '/edit_project', isAuthenticatedAccessToken, function (req, res) {
        var data = req.body;
        var eventpath = req.files;
        ProjectHandler.edit_project(data, eventpath, function (response) {
            response.version = version;
            res.json(response);
        });
    });

    router.post(BASE_API_URL + '/deactivate_project', isAuthenticatedAccessToken, function (req, res) {
        var data = req.body;
        ProjectHandler.deactivate_project(data, function (response) {
            response.version = version;
            res.json(response);
        });
    });

    router.get(BASE_API_URL + '/get_projects_project_status', function (req, res) {
        var data = req.query;
        ProjectHandler.get_projects_project_status(data, function (response) {
            response.version = version;
            res.json(response);
        });
    });

    router.get(BASE_API_URL + '/get_projects_project_status', function (req, res) {
        var data = req.query;
        ProjectHandler.get_projects_project_status(data, function (response) {
            response.version = version;
            res.json(response);
        });
    });

    router.post(BASE_API_URL + '/projects_filter', function (req, res) {
        var data = req.body;
        console.log("insisisssi")
        ProjectHandler.projects_filter(data, function (response) {
            response.version = version;
            res.json(response);
        });
    });

    router.post(BASE_API_URL + '/complaints_filter', function (req, res) {
        var data = req.body;
        ComplaintHandler.complaints_filter(data, function (response) {
            response.version = version;
            res.json(response);
        });
    });



    router.get(BASE_API_URL + '/help_desk', function (req, res) {
        var response = {};
        var category = [{
            "help_desk_name": "Hospital",
            "contact_number": "9999999999"
        },
        {
            "help_desk_name": "Police-Station",
            "contact_number": "100"
        },
        {
            "help_desk_name": "Ambulance",
            "contact_number": "108"
        },
        {
            "help_desk_name": "Fire Service",
            "contact_number": "8888888888"
        }
        ];
        response.help_desk_contacts = category;
        response.statuscode = 200;
        res.json(response);
    });


    router.post(BASE_API_URL + '/user_filter', function (req, res) {
        var data = req.body;
        UserHandler.user_filter(data, function (response) {
            response.version = version;
            res.json(response);
        });
    });

    router.post(BASE_API_URL + '/meeting_filter', function (req, res) {
        var data = req.body;
        MeetingRequestHandler.meeting_filter(data, function (response) {
            response.version = version;
            res.json(response);
        });
    });

    router.get(BASE_API_URL + '/list_meetings_by_user_id', function (req, res) {
        var data = req.query;
        MeetingRequestHandler.list_meetings_by_user_id(data, function (response) {
            response.version = version;
            res.json(response);
        });
    });


    router.post(BASE_API_URL + '/send_message', isAuthenticatedAccessToken, function (req, res) {
        var data = req.body;
        UserHandler.send_message(data, function (response) {
            response.version = version;
            res.json(response);
        });
    });

    router.get(BASE_API_URL + '/birthday_message', function (req, res) {
        UserHandler.birthday_message(function (response) {
            response.version = version;
            res.json(response);
        });
    });

    // Department
    router.post(BASE_API_URL + '/add_department_type', isAuthenticatedAccessToken, function (req, res) {
        var data = req.body;
        DepartmentHandler.add_department_type(data, function (response) {
            response.version = version;
            res.json(response);
        });
    });

    router.post(BASE_API_URL + '/add_department_contact', isAuthenticatedAccessToken, function (req, res) {
        var data = req.body;
        var eventpath = req.files;
        DepartmentHandler.add_department_contact(data, eventpath, function (response) {
            response.version = version;
            res.json(response);
        });
    });

    router.post(BASE_API_URL + '/edit_department_contact', isAuthenticatedAccessToken, function (req, res) {
        var data = req.body;
        var eventpath = req.files;
        DepartmentHandler.edit_department_contact(data, eventpath, function (response) {
            response.version = version;
            res.json(response);
        });
    });

    router.get(BASE_API_URL + '/get_department_contact', isAuthenticatedAccessToken, function (req, res) {
        var data = req.query;
        DepartmentHandler.get_department_contact(data, function (response) {
            response.version = version;
            res.json(response);
        });
    });

    router.post(BASE_API_URL + '/deactivate_department_contact', isAuthenticatedAccessToken, function (req, res) {
        var data = req.body;
        DepartmentHandler.deactivate_department_contact(data, function (response) {
            response.version = version;
            res.json(response);
        });
    });

    router.post(BASE_API_URL + '/list_departmentcontact_by_department', isAuthenticatedAccessToken, function (req, res) {
        var data = req.body;
        DepartmentHandler.list_departmentcontact_by_department(data, function (response) {
            response.version = version;
            res.json(response);
        });
    });

    router.post(BASE_API_URL + '/list_departmentcontact', isAuthenticatedAccessToken, function (req, res) {
        var data = req.body;
        DepartmentHandler.list_departmentcontact(data, function (response) {
            response.version = version;
            res.json(response);
        });
    });

    router.get(BASE_API_URL + '/list_all_departmentcontact', isAuthenticatedAccessToken, function (req, res) {
        DepartmentHandler.list_all_departmentcontact(function (response) {
            response.version = version;
            res.json(response);
        });
    });

    // Party worker
    router.post(BASE_API_URL + '/add_partyworker', isAuthenticatedAccessToken, function (req, res) {
        var data = req.body;
        var eventpath = req.files;
        PartyworkerHandler.add_partyworker(data, eventpath, function (response) {
            response.version = version;
            res.json(response);
        });
    });

    router.post(BASE_API_URL + '/edit_partyworker', isAuthenticatedAccessToken, function (req, res) {
        var data = req.body;
        var eventpath = req.files;
        PartyworkerHandler.edit_partyworker(data, eventpath, function (response) {
            response.version = version;
            res.json(response);
        });
    });

    router.get(BASE_API_URL + '/get_partyworker', isAuthenticatedAccessToken, function (req, res) {
        var data = req.query;
        PartyworkerHandler.get_partyworker(data, function (response) {
            response.version = version;
            res.json(response);
        });
    });

    router.post(BASE_API_URL + '/deactivate_partyworker', isAuthenticatedAccessToken, function (req, res) {
        var data = req.body;
        PartyworkerHandler.deactivate_partyworker(data, function (response) {
            response.version = version;
            res.json(response);
        });
    });

    router.post(BASE_API_URL + '/list_partyworkers_by_workerstype', isAuthenticatedAccessToken, function (req, res) {
        var data = req.body;
        PartyworkerHandler.list_partyworkers_by_workerstype(data, function (response) {
            response.version = version;
            res.json(response);
        });
    });

    router.post(BASE_API_URL + '/partyworker_filter', isAuthenticatedAccessToken, function (req, res) {
        var data = req.body;
        PartyworkerHandler.partyworker_filter(data, function (response) {
            response.version = version;
            res.json(response);
        });
    });

    router.get(BASE_API_URL + '/list_partyworkers', isAuthenticatedAccessToken, function (req, res) {
        PartyworkerHandler.list_partyworkers(function (response) {
            response.version = version;
            res.json(response);
        });
    });

    router.get(BASE_API_URL + '/list_events', isAuthenticatedAccessToken, function (req, res) {
        EventHandler.list_events(function (response) {
            response.version = version;
            res.json(response);
        });
    });

    router.get(BASE_API_URL + '/list_projects', isAuthenticatedAccessToken, function (req, res) {
        ProjectHandler.list_projects(function (response) {
            response.version = version;
            res.json(response);
        });
    });

    router.get(BASE_API_URL + '/list_news', isAuthenticatedAccessToken, function (req, res) {
        NewsHandler.list_news(function (response) {
            response.version = version;
            res.json(response);
        });
    });

    router.get(BASE_API_URL + '/list_complaints', isAuthenticatedAccessToken, function (req, res) {
        ComplaintHandler.list_complaints(function (response) {
            response.version = version;
            res.json(response);
        });
    });

    router.get(BASE_API_URL + '/list_meeting_requests', isAuthenticatedAccessToken, function (req, res) {
        MeetingRequestHandler.list_meeting_requests(function (response) {
            response.version = version;
            res.json(response);
        });
    });

    router.get(BASE_API_URL + '/list_users', isAuthenticatedAccessToken, function (req, res) {
        UserHandler.list_users(function (response) {
            response.version = version;
            res.json(response);
        });
    });

    router.get(BASE_API_URL + '/dashboard_count', isAuthenticatedAccessToken, function (req, res) {
        CountHandler.dashboard_count(function (response) {
            response.version = version;
            res.json(response);
        });
    });


    return router;
};
