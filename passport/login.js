var LocalStrategy = require('passport-local').Strategy;
var bCrypt = require('bcrypt-nodejs');
var jwt = require('jsonwebtoken');
var configurations = require("../config");
var app = require('../app')

module.exports = function (passport) {
    passport.use('login', new LocalStrategy({
        usernameField: 'username',
        passwordField: 'password',
        passReqToCallback: true
    }, function (req, username, password, done) {
        var data = req.body;
        var date = new Date().toUTCString();
        var db = app.db;
        const usersCollection = db.collection("users");
        let query = usersCollection.where('username', '==', username).get()
            .then(snapshot => {
                if (snapshot.empty) {
                    return done(null, false, {
                        'message': 'User Not found.'
                    });
                } else {
                    snapshot.forEach(doc => {
                        var docid = doc.id
                        var docdata = doc.data()
                        if (docdata.is_active == false) {
                            return done(null, false, {
                                'message': 'User Account has been deactivated'
                            });

                        } else {
                            var encryptpassword = bCrypt.compareSync(password.toString(), docdata.password);
                            if (encryptpassword == false) {
                                return done(null, false, {
                                    'msg': 'Invalid Password'
                                });
                            } else {
                                var userdata = doc.data();
                                userdata.expiresAt = null;
                                userdata.jwt_token = ""
                                var jwt_object = {};
                                jwt_object.username = userdata.username;
                                jwt_object.user_id = docid;
                                var jwt_token = jwt.sign({
                                    data: jwt_object
                                }, configurations.TOKEN_SECRET);
                                userdata.jwt_token = jwt_token;
                                let newUser = {
                                    "username": username,
                                    "jwt_token": userdata.jwt_token,
                                    "is_active": userdata.is_active,
                                    "role": userdata.role,
                                    // "email_verified": true,
                                    "isLoggedin": true
                                }
                                return done(null, newUser);
                            }
                        }
                    });
                }
            })
    }));
};