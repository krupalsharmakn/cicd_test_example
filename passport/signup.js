var LocalStrategy = require('passport-local').Strategy;
var bCrypt = require('bcrypt-nodejs');
var app = require('../app')
var crypto = require('crypto');



module.exports = function (passport) {
    passport.use('signup', new LocalStrategy({
        usernameField: 'username',
        passwordField: 'password',
        passReqToCallback: true
    }, function (req, username, password, done) {
        var db = app.db;
        const usersCollection = db.collection("users");
        findOrCreateUser = function () {
            var data = req.body;
            let query = usersCollection.where('username', '==', req.body.username).get()
                .then(snapshot => {
                    if (snapshot.empty) {
                        let docId = Math.floor(Math.random() * (99999 - 00000)); //need ID for the document
                        var name = username;
                        var signupcry = crypto.createHash('md5').update(name).digest('hex');
                        var time = new Date().getTime();
                        var passtoken = signupcry.concat(time);
                        let newUser = {
                            "username": username,
                            "password": createHash(password),
                            "passtoken": passtoken,
                            "is_active": true,
                            "email_verified": true,
                            "role": "user"
                        }
                        let setNewUser = usersCollection.doc(String(docId)).set(newUser);
                        return done(null, newUser);
                    } else {
                        snapshot.forEach(doc => {
                            return done({
                                msg: "Username " + username
                            }, false, {
                                msg: req.flash(username + " is already taken.")
                            });
                        });
                    }
                })
        };
        process.nextTick(findOrCreateUser);
    }));

    /* Generates hash using bCrypt*/
    var createHash = function (password) {
        return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
    }

}