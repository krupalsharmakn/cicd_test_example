var config = require("../config");
var AWS = require('aws-sdk');
var fs = require('fs');
var uuidV4 = require("../utils/utils");

const s3 = new AWS.S3({
    accessKeyId: config.awsS3.accessKeyId,
    secretAccessKey: config.awsS3.secretAccessKey,
    region: config.awsS3.region,
    folder1: config.awsS3.folder1
});
const myBucket = config.awsS3.bucket1;

module.exports = {
    uploadFile: async function (data, callback) {
        let sampleFile = data;
        var date = new Date();
        var d = date.getTime();
        var fileExtension = data.name.substr((data.name.lastIndexOf('.') + 1));
        var unique_id = uuidV4.getUniqueId();
        var filename = d + unique_id + "." + fileExtension;
        await sampleFile.mv('images/google/' + filename, async function (err) {
            s3.upload({
                Key: filename,
                Bucket: myBucket,
                ContentType:'image/'+fileExtension,
                ACL: "public-read",
                Body: fs.createReadStream('images/google/' + filename)
            }, function (err, output) {
                if (output) {
                    console.log("Finished uploading:", output.Location);
                    fs.unlink('images/google/' + filename, function (err) {
                        if (err) throw err;
                        console.log('File deleted!');
                    });
                } else {
                    console.log('Not uploaded!');
                }

            });
        });
        return Promise.resolve(filename);
    },

    getFile: async function (data, callback) {
        var myKey = data;
        const signedUrlExpireSeconds = 60 * 5;
        const url = await s3.getSignedUrl('getObject', {
            Bucket: myBucket,
            Key: myKey,
            Expires: signedUrlExpireSeconds
        })
        console.log(url)
        return url;
    },

    deleteFile: function (data, callback) {
        var myKey = data;
        if(myKey){
            s3.deleteObject({
                Bucket: myBucket,
                Key: myKey
            }, function (err, data) {
                if (data) {
                    console.log("Deleted successfully");
                } else {
                    console.log("error");
                }
            });
        } else {
            console.log("myKey not found");
        }

    }
};
