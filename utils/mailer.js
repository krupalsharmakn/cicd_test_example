var nodemailer = require('nodemailer');
const sgMail = require('@sendgrid/mail');
sgMail.setApiKey('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');
var config = require("../config");
var transporter = nodemailer.createTransport({
    service: 'Gmail',
    auth: {
        user: config.SENDER_EMAIL,
        pass: config.EMAIL_PASSWORD
    }
});

module.exports = {

    //Send Mail using nodemailer
    sendMailNotification: function (data) {
        // setup e-mail data with unicode symbols
        var mailOptions = {
            from: data.from, // sender address
            to: data.to, // list of receivers
            subject: data.subject, // Subject line
            html: data.html
        };
        // send mail with defined transport object
        transporter.sendMail(mailOptions, function (error, info) {
            if (error) {
                console.log('Error sending message: ' + error);
            } else {
                console.log('Message sent: ' + info.response);
            }
        });
    },

    // //Send Mail using Sendgrid
    // sendMailNotification: function (data) {

    //     var mailOptions = {
    //         from: data.from, // sender address      'krupal.s@avohi.com',
    //         to: data.to, // list of receivers       'krupalsharmakn1@gmail.com',
    //         subject: data.subject, //Subjectline    'Sending with SendGrid is Fun',
    //         html: data.html        //html           '<strong>And easy to do anywhere, even with Node.js</strong>',
    //     };
    //   // send mail with defined transport object
    //   transporter.sendMail(mailOptions, function (error, info) {
    //     if (error) {
    //         console.log('Error sending message: ' + error);
    //     } else {
    //         console.log('Message sent: ' + info.response);
    //     }
    // });
    // },
};
