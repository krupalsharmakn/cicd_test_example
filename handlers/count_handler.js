var app = require("../app");

module.exports = {

    dashboard_count: function (callback) {
        Dashboard_countData = function () {
            var activecount = {
                "users_count": "",
                "events_count": "",
                "complaints_count": "",
                "news_count": "",
                "projects_count": "",
                "partyworker_count": "",
                "departmentcontact_count": "",
                "meeting_requests_count":""
            };

            var db = app.db;
            const user_count = db.collection("users");
            let query = user_count.get()
                .then(snapshot => {
                    //console.log(snapshot.size)
                    activecount.users_count = snapshot.size - 1;
                    const event_count = db.collection("event");
                    let query = event_count.get()
                        .then(snapshot => {
                            //console.log(snapshot.size)
                            activecount.events_count = snapshot.size;
                            const complaints_count = db.collection("complaints");
                            let query = complaints_count.get()
                                .then(snapshot => {
                                    //console.log(snapshot.size)
                                    activecount.complaints_count = snapshot.size;
                                    const news_count = db.collection("news");
                                    let query = news_count.get()
                                        .then(snapshot => {
                                            //console.log(snapshot.size)
                                            activecount.news_count = snapshot.size;
                                            const projects_count = db.collection("projects");
                                            let query = projects_count.get()
                                                .then(snapshot => {
                                                    //console.log(snapshot.size)
                                                    activecount.projects_count = snapshot.size;
                                                    const partyworker_count = db.collection("partyworker");
                                                    let query = partyworker_count.get()
                                                        .then(snapshot => {
                                                            //console.log(snapshot.size)
                                                            activecount.partyworker_count = snapshot.size;
                                                            const departmentcontact_count = db.collection("departmentcontact");
                                                            let query = departmentcontact_count.get()
                                                                .then(snapshot => {
                                                                   // console.log(snapshot.size)
                                                                    activecount.departmentcontact_count = snapshot.size;
                                                                    const meeting_requests_count = db.collection("meeting_requests");
                                                                    let query = meeting_requests_count.get()
                                                                        .then(snapshot => {
                                                                            //console.log(snapshot.size)
                                                                            activecount.meeting_requests_count = snapshot.size;
                                                                            callback({
                                                                                statuscode: 200,
                                                                                msg: "Dashboard Counts",
                                                                                data: activecount
                                                                            });
                                                                        })
                                                                })
                                                        })
                                                })
                                        })
                                })
                        })
                })
        }
        process.nextTick(Dashboard_countData);
    }
}
