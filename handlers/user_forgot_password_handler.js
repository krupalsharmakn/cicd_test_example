var config = require("../config");
var app = require('../app')
var randomize = require('randomatic');

module.exports = {
    user_forgot_password: function (data, callback) {
        user_forgotpassword_data = function () {
            if (data.mobile_number) {
                var db = app.db;
                const usersCollection = db.collection("users");
                var random_number = randomize('Aa0', 6);
                let query = usersCollection.where('mobile_number', '==', data.mobile_number).get()
                    .then(snapshot => {
                        if (snapshot.empty) {
                            callback({
                                "statuscode": 404,
                                "msg": "User Not Found"
                            });
                        } else {
                            snapshot.forEach(doc => {
                                var docdata = doc.data();
                                docdata.otp = random_number;
                                usersCollection.doc(doc.id).update(docdata);
                                var msgBody = "OTP for forgot password ,MLA-Application is = " + random_number + " \n If it is not you then, kindly ignore this message. \n Thank You .";
                                var mobile_number = docdata.mobile_number;
                                var msg91 = require("msg91")(config.msg_id, config.msg_name, "4");
                                msg91.send(mobile_number, msgBody, function (err, response) {});
                                callback({
                                    msg: "forgotpassword success. OTP has been sent successfully to the registered mobile number",
                                    statuscode: 204
                                });

                            });
                        }
                    })
            } else {
                callback({
                    "statuscode": 404,
                    "msg": "Mobile number not Found"
                });
            }
        }
        process.nextTick(user_forgotpassword_data);
    }
}