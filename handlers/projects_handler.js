var uuidV4 = require("../utils/utils");
var async = require('async');
var config = require("../config");
var app = require("../app");
var FCM = require('fcm-push');
var fcm = new FCM(config.FCM_KEY);
var fs = require('fs');
var AWS_S3 = require("../utils/aws-s3");

module.exports = {
    add_project: function (data, eventpath, callback) {
        project_data = function () {
            var unique_id = uuidV4.getUniqueId();
            var db = app.db;
            const addproject = db.collection("projects");
            let query = addproject.where('project_id', '==', unique_id).get()
                .then(async snapshot => {
                    if (snapshot.empty) {
                        var reports;
                        var files = [];
                        if (eventpath == null) {
                            files = [];
                        } else {
                            if (Array.isArray(eventpath.image)) {
                                var fileArray = (eventpath.image).concat();
                            } else {
                                var fileArray = []
                                fileArray.push(eventpath.image)
                            }
                        }
                        if (eventpath != null) {
                            for (var i = 0; i < fileArray.length; i++) {
                                let sampleFile = fileArray[i];
                                console.log("sampleFile = ",sampleFile)
                                var date = new Date();
                                var d = date.getTime();
                                var fileExtension = sampleFile.name.substr((sampleFile.name.lastIndexOf('.') + 1));
                                var unique_id = uuidV4.getUniqueId();
                                var filename = d + unique_id + "." + fileExtension;
                                await sampleFile.mv(config.image_path + filename);
                                var imagepath = config.get_image_path + filename;
                                files.push(imagepath);
                            }
                        } else {
                            files = []
                        }
                        let add_project = {
                            "project_id": unique_id,
                            "project_title": data.project_title,

                            "project_description": data.project_description,
                            "project_start_date": data.project_start_date,
                            "project_end_date": data.project_end_date,
                            "project_images": files,
                            "is_active": true,
                            "project_status": data.project_status, //ongoing,completed
                            "created_at": new Date(Date.now()).toISOString()
                        }
                        addproject.doc().set(add_project);
                        add_project = JSON.parse(JSON.stringify(add_project))

                        const getusers = db.collection("users");
                        getusers.get()
                            .then(snapshot => {
                                if (snapshot.empty) {
                                    callback({
                                        statuscode: 200,
                                        msg: "Project added successfully",
                                        data: add_project
                                    });
                                } else {
                                    var user_list = []
                                    snapshot.forEach(async doc => {
                                        UserData = doc.data();
                                        user_list.push(UserData)
                                    });
                                    for (var i = 0; i < user_list.length; i++) {
                                        var message = {};
                                        message.to = user_list[i].fcmtoken;
                                        message.collapse_key = "Personal Diary";
                                        var notification = {};
                                        notification.title = "New Project added";
                                        notification.body = "New Project has arrived .";
                                        message.notification = notification;
                                        var data = {};
                                        data = add_project;
                                        message.data = data;
                                        message = JSON.stringify(message)
                                        message = JSON.parse(message)

                                        console.log("message = ", message);
                                        fcm.send(message, function (err, messageId) {

                                            if (messageId) {
                                                console.log("message is sent");
                                            }
                                        });
                                        if (i == user_list.length - 1) {
                                            callback({
                                                statuscode: 200,
                                                msg: "Project added successfully",
                                                data: add_project
                                            });
                                        }
                                    }

                                }
                            })

                    } else {
                        callback({
                            statuscode: 304,
                            msg: "project_id exists",
                            data: doc.data()
                        });
                    }

                })

        }
        process.nextTick(project_data);
    },

    get_project: function (data, callback) {
        project_data = function () {
            if (data.project_id) {
                var db = app.db;
                const getproject = db.collection("projects");
                let query = getproject.where('project_id', '==', data.project_id).get()
                    .then(snapshot => {
                        if (snapshot.empty) {
                            callback({
                                statuscode: 404,
                                msg: "project_id not found"
                            });
                        } else {
                            snapshot.forEach(async doc => {
                                docdata = doc.data();
                                callback({
                                    statuscode: 200,
                                    msg: "project details",
                                    data: docdata
                                });
                            });
                        }
                    })
            } else {
                callback({
                    statuscode: 404,
                    msg: "project_id not found"
                });
            }
        }
        process.nextTick(project_data);
    },

    edit_project: function (data, eventpath, callback) {
        project_data = function () {
            if (data.project_id) {
                var db = app.db;
                const editproject = db.collection("projects");
                let query = editproject.where('project_id', '==', data.project_id).get()
                    .then(snapshot => {
                        if (snapshot.empty) {
                            callback({
                                statuscode: 404,
                                msg: "project_id not found"
                            });
                        } else {
                            snapshot.forEach(async doc => {
                                docdata = doc.data();
                                var old_images = docdata.project_images;
                                var reports;
                                var files = [];
                                if (eventpath) {
                                    if (Array.isArray(eventpath.image)) {
                                        var fileArray = (eventpath.image).concat();
                                    } else {
                                        var fileArray = []
                                        fileArray.push(eventpath.image)
                                    }
                                    for (var i = 0; i < fileArray.length; i++) {
                                        let sampleFile = fileArray[i];
                                        var date = new Date();
                                        var d = date.getTime();
                                        var fileExtension = sampleFile.name.substr((sampleFile.name.lastIndexOf('.') + 1));
                                        var unique_id = uuidV4.getUniqueId();
                                        var filename = d + unique_id + "." + fileExtension;
                                        await sampleFile.mv(config.image_path + filename);
                                        var imagepath = config.get_image_path + filename;

                                        files.push(imagepath);
                                        if (fileArray.length - 1 == i) {
                                            docdata.project_images = files;
                                            for (var i = 0; i < old_images.length; i++) {
                                                var oldimage = old_images[i].split('/');
                                                var last = oldimage[oldimage.length - 1]
                                                fs.unlink(config.image_path + last);
                                            }
                                        }
                                    }
                                } else {
                                    docdata.achievement_images = old_images;
                                }
                                if (data.project_title !== undefined && data.project_title !== "") {
                                    docdata.project_title = data.project_title;
                                }
                                if (data.project_description !== undefined && data.project_description !== "") {
                                    docdata.project_description = data.project_description;
                                }
                                if (data.project_start_date !== undefined && data.project_start_date !== "") {
                                    docdata.project_start_date = data.project_start_date;
                                }
                                if (data.project_end_date !== undefined && data.project_end_date !== "") {
                                    docdata.project_end_date = data.project_end_date;
                                }
                                if (data.project_status !== undefined && data.project_status !== "") {
                                    docdata.project_status = data.project_status;
                                }

                                editproject.doc(doc.id).update(docdata);
                                callback({
                                    statuscode: 200,
                                    msg: "project details updated successfully",
                                    data: docdata
                                });
                            });
                        }
                    })
            } else {
                callback({
                    statuscode: 404,
                    msg: "project_id not found"
                });
            }
        }
        process.nextTick(project_data);
    },

    deactivate_project: function (data, callback) {
        project_data = function () {
            if (data.project_id) {
                var db = app.db;
                const deactivate = db.collection("projects");
                deactivate.where('project_id', '==', data.project_id).get()
                    .then(snapshot => {
                        if (snapshot.empty) {
                            callback({
                                statuscode: 404,
                                msg: "project_id not found"
                            });
                        } else {
                            snapshot.forEach(doc => {
                                docdata = doc.data();
                                if (data.is_active == true) {
                                    docdata.is_active = true
                                }
                                if (data.is_active == false) {
                                    docdata.is_active = false
                                }
                                deactivate.doc(doc.id).update(docdata);
                                callback({
                                    statuscode: 200,
                                    msg: " project deactivated successfully"
                                });
                            });
                        }
                    })
            } else {
                callback({
                    statuscode: 404,
                    msg: "project_id not found"
                });
            }
        }
        process.nextTick(project_data);
    },


    // db.collection("****").where('display', '==', true).where('createdAt', '>', today).get().then(function(querySnapshot) {
    get_projects_project_status: function (data, callback) {
        project_data = function () {
            if (data.project_status) {
                var db = app.db;
                const getproject = db.collection("projects");
                let query = getproject.where('project_status', '==', data.project_status).where('is_active', '==', true).get()
                    .then(snapshot => {
                        if (snapshot.empty) {
                            callback({
                                statuscode: 404,
                                msg: "No projects found"
                            });
                        } else {
                            var arr=[]
                            snapshot.forEach(async doc => {
                                docdata = doc.data();
                               arr.push(docdata)
                            });
                            callback({
                                statuscode: 200,
                                msg: "project details",
                                data: arr
                            });
                        }
                    })
            } else {
                callback({
                    statuscode: 404,
                    msg: "project_status not found"
                });
            }
        }
        process.nextTick(project_data);
    },


   projects_filter: function (data, callback) {
        project_data = function () {
            var docvalue = [];
            var enddata;
            var db = app.db;
var active_status;
            const getproject = db.collection("projects");

var is_active=JSON.parse(data.is_active);
console.log("is_active",is_active);
if(data.is_active && data.project_status=="" && data.project_start_date==""){

 getproject.where('is_active', '==', is_active).get()
                    .then(snapshot => {

                        if (snapshot.empty) {
                            callback({
                                statuscode: 404,
                                msg: "No projects found"
                            });
                        } else {
                            var arr = []
                            snapshot.forEach(async doc => {
                                docvalue = doc.data();
                                arr.push(docvalue)
                            });
                            callback({
                                statuscode: 200,
                                msg: "project details",
                                data: arr
                            });
                        }
                    })


}
            if (data.project_status && data.project_start_date == "") {
                getproject.where('is_active', '==', is_active).where('project_status', '==', data.project_status).get()
                    .then(snapshot => {

                        if (snapshot.empty) {
console.log("snapshot empty");
                            callback({
                                statuscode: 404,
                                msg: "No projects found"
                            });
                        } else {
console.log("snapshot empty 11111");
                            var arr = []
                            snapshot.forEach(async doc => {
                                docvalue = doc.data();
                                arr.push(docvalue)
                            });
                            callback({
                                statuscode: 200,
                                msg: "project details",
                                data: arr
                            });
                        }
                    })


            }
            if (data.project_start_date && data.project_end_date) {


                let query = getproject.where('is_active', '==', is_active).where('project_start_date', '>=', data.project_start_date).get()
                    .then(snapshot => {
                        if (snapshot.empty) {
                            callback({
                                statuscode: 404,
                                msg: "No projects found"
                            });
                        } else {
                            var arr = []
                            snapshot.forEach(async doc => {
                                docvalue = doc.data();
                                if (docvalue.project_end_date <= data.project_end_date) {
                                    enddata = docvalue;
                                    arr.push(enddata)
                                }
                            });
                            var filterdata = arr.filter(arrayactive);
                            if (data.project_status) {
                                filterdata = filterdata.filter(arrayprojectstatus)
                            }
                            callback({
                                statuscode: 200,
                                msg: "project details",
                                data: filterdata
                            });
                        }
                    })
            }

            function arrayprojectstatus(arraydata) {
                if (arraydata.project_status == data.project_status) {
                    var truedata = arraydata;
                    return truedata;

                }

            }
            // else {
            // callback({
            // statuscode: 404,
            // msg: "projects not found"
            // });
            // } 
        }
        process.nextTick(project_data);
    },

    list_projects: function (callback) {
        ProjectsData = function () {
            var db = app.db;
            const projects_list = db.collection("projects");
            let query = projects_list.get()
                .then(snapshot => {
                    if (snapshot.empty) {
                        callback({
                            statuscode: 404,
                            msg: "Projects not found"
                        });
                    } else {
                        var projects_list = []
                        snapshot.forEach(async doc => {
                            docdata = doc.data();
                            projects_list.push(docdata)

                        });
                        callback({
                            statuscode: 200,
                            msg: "Projects List",
                            data: projects_list
                        });
                    }
                })
        }
        process.nextTick(ProjectsData);
    }
}

function arrayactive(arraydata) {
    if (arraydata.is_active == true) {
        var truedata = arraydata;
        return truedata;

    }

}