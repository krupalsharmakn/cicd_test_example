var uuidV4 = require("../utils/utils");
var async = require('async');
var config = require("../config");
var app = require("../app");
var fs = require('fs');

module.exports = {

    add_mla_profile: function (data, eventpath, callback) {
        profileData = function () {
            var mla_profile_id = uuidV4.getUniqueId();
            var db = app.db;
            const mlaprofile = db.collection("mla_profile");
            let query = mlaprofile.where('mla_profile_id', '==', mla_profile_id).get()
                .then(async snapshot => {
                    if (snapshot.empty) {
                        var reports;
                        var files = "";
                        if (eventpath == null) {
                            files = "";
                        } else {
                            files = eventpath.image
                            let sampleFile = eventpath.image
                            var date = new Date();
                            var d = date.getTime();
                            var fileExtension = sampleFile.name.substr((sampleFile.name.lastIndexOf('.') + 1));
                            var unique_id = uuidV4.getUniqueId();
                            var filename = d + unique_id + "." + fileExtension;
                            await sampleFile.mv(config.image_path + filename);
                            reports = config.get_image_path + filename;
                        }
                        let add_profile = {
                            "mla_profile_id": mla_profile_id,
                            "mla_name": data.mla_name,
                            //"mla_name_t": data.mla_name_t,
                            "party_name": data.party_name,
                            //"party_name_t": data.party_name_t,
                            "constituency_name": data.constituency_name,
                            //"constituency_name_t": data.constituency_name_t,
                            "profile_description": data.profile_description,
                            //"profile_description_t": data.profile_description_t,
                            "profile_image": reports,
                            "address": data.address,
                            //"address_t": data.address_t,
                            "public_meeting_timings": data.public_meeting_timings,
                            "mail_id": data.mail_id,
                            "facebook_id": data.facebook_id,
                            "instagram_id": data.instagram_id,
                            "linkedin_id": data.linkedin_id,
                            "twitter_id": data.twitter_id,
                            "youtube_id": data.twitter_id,
                            "secretaries": data.secretaries,
                            "is_active": true,
                            "created_at": new Date(Date.now()).toISOString()
                        }
                        mlaprofile.doc().set(add_profile);
                        callback({
                            statuscode: 200,
                            msg: "mla-profile added successfully",
                            data: add_profile
                        });
                    } else {

                        callback({
                            statuscode: 304,
                            msg: "mla_profile_id exists"
                        });

                    }
                })
        }
        process.nextTick(profileData);
    },

    edit_mla_profile: function (data, eventpath, callback) {
        profileData = function () {
            if (data.mla_profile_id) {
                var db = app.db;
                const mlaprofile = db.collection("mla_profile");
                let query = mlaprofile.where('mla_profile_id', '==', data.mla_profile_id).get()
                    .then(snapshot => {
                        if (snapshot.empty) {
                            callback({
                                statuscode: 404,
                                msg: "mla_profile_id not found"
                            });
                        } else {
                            snapshot.forEach(async doc => {
                                docdata = doc.data();
                                var old_images = docdata.profile_image;
                                var reports;
                                var files = "";
                                if (eventpath) {
                                    let sampleFile = eventpath.image;
                                    var date = new Date();
                                    var d = date.getTime();
                                    var fileExtension = sampleFile.name.substr((sampleFile.name.lastIndexOf('.') + 1));
                                    console.log(" fileExtension =", fileExtension)
                                    var unique_id = uuidV4.getUniqueId();
                                    var filename = d + unique_id + "." + fileExtension;
                                    console.log(" filename =", filename)
                                    await sampleFile.mv(config.image_path + filename);
                                    reports = config.get_image_path + filename;
                                    docdata.profile_image = reports;
                                    console.log("docdata.profile_image =", docdata.profile_image)
                                    var oldimage = old_images.split('/');
                                    var last = oldimage[oldimage.length - 1]

                                    fs.unlink(config.image_path + last);
                                } else {
                                    docdata.profile_image = old_images;
                                }
                                if (data.mla_name !== undefined && data.mla_name !== "") {
                                    docdata.mla_name = data.mla_name;
                                }
                                if (data.mla_name_t !== undefined && data.mla_name_t !== "") {
                                    docdata.mla_name_t = data.mla_name_t;
                                }
                                if (data.party_name !== undefined && data.party_name !== "") {
                                    docdata.party_name = data.party_name;
                                }
                                if (data.party_name_t !== undefined && data.party_name_t !== "") {
                                    docdata.party_name_t = data.party_name_t;
                                }
                                if (data.constituency_name !== undefined && data.constituency_name !== "") {
                                    docdata.constituency_name = data.constituency_name;
                                }
                                if (data.constituency_name_t !== undefined && data.constituency_name_t !== "") {
                                    docdata.constituency_name_t = data.constituency_name_t;
                                }
                                if (data.profile_description !== undefined && data.profile_description !== "") {
                                    docdata.profile_description = data.profile_description;
                                }
                                if (data.profile_description_t !== undefined && data.profile_description_t !== "") {
                                    docdata.profile_description_t = data.profile_description_t;
                                }
                                if (data.address !== undefined && data.address !== "") {
                                    docdata.address = data.address;
                                }
                                if (data.address_t !== undefined && data.address_t !== "") {
                                    docdata.address_t = data.address_t;
                                }
                                if (data.public_meeting_timings !== undefined && data.public_meeting_timings !== "") {
                                    docdata.public_meeting_timings = data.public_meeting_timings;
                                }
                                if (data.twitter_id !== undefined && data.twitter_id !== "") {
                                    docdata.twitter_id = data.twitter_id;
                                }
                                if (data.mail_id !== undefined && data.mail_id !== "") {
                                    docdata.mail_id = data.mail_id;
                                }
                                if (data.facebook_id !== undefined && data.facebook_id !== "") {
                                    docdata.facebook_id = data.facebook_id;
                                }
                                if (data.youtube_id !== undefined && data.youtube_id !== "") {
                                    docdata.youtube_id = data.youtube_id;
                                }
                                if (data.instagram_id !== undefined && data.instagram_id !== "") {
                                    docdata.instagram_id = data.instagram_id;
                                }
                                if (data.linkedin_id !== undefined && data.linkedin_id !== "") {
                                    docdata.linkedin_id = data.linkedin_id;
                                }
                                // if (data.social_media !== undefined && data.social_media !== "") {
                                //     docdata.social_media = data.social_media;
                                // }
                                if (data.secretaries !== undefined && data.secretaries !== "") {
                                    docdata.secretaries = data.secretaries;
                                }


                                console.log("docdata", docdata)
                                mlaprofile.doc(doc.id).update(docdata);
                                callback({
                                    statuscode: 200,
                                    msg: "mla profile updated successfully",
                                    data: docdata
                                });

                            });
                        }
                    })
            } else {
                callback({
                    statuscode: 404,
                    msg: "mla_profile_id not found"
                });
            }
        }
        process.nextTick(profileData);
    },


    new_add_banner_images: function (eventpath, callback) {
        image_data =  function () {
            const imageFolder = "./images/banner_images"
            var image_list = [];
            fs.readdirSync(imageFolder).forEach(file => {
                newimage = config.get_banner_image + file
                image_list.push(newimage)
            });
            var old_images=[];
            old_images = image_list;
            var files = [];
            if (eventpath) {
                if (Array.isArray(eventpath.image)) {
                    var fileArray = (eventpath.image).concat();
                } else {
                    var fileArray = []
                    fileArray.push(eventpath.image)
                }
                for (var i = 0; i < fileArray.length; i++) {
                    let sampleFile = fileArray[i];
                    var date = new Date();
                    var d = date.getTime();
                    var fileExtension = sampleFile.name.substr((sampleFile.name.lastIndexOf('.') + 1));
                    var unique_id = uuidV4.getUniqueId();
                    var filename = d + unique_id + "." + fileExtension;
                     sampleFile.mv(config.banner_image_path + filename);
                    var imagepath = config.get_banner_image + filename;
                    files.push(imagepath);
                    if (fileArray.length - 1 == i) {
                        image_list = files;
                        if(old_images){
                        for (var i = 0; i < old_images.length; i++) {
                            var oldimage = old_images[i].split('/');
                            var last = oldimage[oldimage.length - 1]
                            fs.unlink(config.banner_image_path + last);
                        }
                    }
                    }
                }
            }

            callback({
                "statuscode": 200,
                "msg": "banner images updated",
                "banner_images": image_list
            })


        }


        process.nextTick(image_data);
    },

add_banner_images: function (data, eventpath, callback) {
image_data = async function () {
console.log("data", typeof (data))
console.log("data", data)
const imageFolder = "./images/banner_images"
var image_list = [];
var bannerimages = [];
if (eventpath || data.deleteimage) {
var reports;
var files = [];
if (eventpath) {
if (Array.isArray(eventpath.image)) {
var fileArray = (eventpath.image).concat();
} else {
var fileArray = []
fileArray.push(eventpath.image)
}
for (var i = 0; i < fileArray.length; i++) {
let sampleFile = fileArray[i];
console.log("sampleFile =", sampleFile.name)
var date = new Date();
var d = date.getTime();
var fileExtension = sampleFile.name.substr((sampleFile.name.lastIndexOf('.') + 1));
console.log(" fileExtension =", fileExtension)
var unique_id = uuidV4.getUniqueId();
var filename = d + unique_id + "." + fileExtension;
console.log(" filename =", filename)
await sampleFile.mv(config.banner_image_path + filename);
var imagepath = config.get_banner_image + filename;
files.push(filename);
}
}
 
if (data.deleteimage) {
var str = data.deleteimage.split("/");
var image = str[str.length - 1]
await fs.unlink(config.banner_image_path + image);
}
 
}
 
fs.readdirSync(imageFolder).forEach(file => {
newimage = config.get_banner_image + file
bannerimages.push(newimage)
});
 
callback({
"statuscode": 200,
"msg": "banner images updated",
"banner_images": bannerimages
})
 
}
 
process.nextTick(image_data);
},


    add_banner_images_old: function (data, eventpath, callback) {
        image_data = async function () {
            const imageFolder = "./images/banner_images"
            var image_list = []
            if (eventpath || data) {
                var reports;
                var files = [];
                if (eventpath.image) {
                    if (Array.isArray(eventpath.image)) {
                        var fileArray = (eventpath.image).concat();
                    } else {
                        var fileArray = []
                        fileArray.push(eventpath.image)
                    }
                    for (var i = 0; i < fileArray.length; i++) {
                        let sampleFile = fileArray[i];
                        console.log("sampleFile =", sampleFile.name)
                        var date = new Date();
                        var d = date.getTime();
                        var fileExtension = sampleFile.name.substr((sampleFile.name.lastIndexOf('.') + 1));
                        console.log(" fileExtension =", fileExtension)
                        var unique_id = uuidV4.getUniqueId();
                        var filename = d + unique_id + "." + fileExtension;
                        console.log(" filename =", filename)
                        await sampleFile.mv(config.banner_image_path + filename);
                        var imagepath = config.get_banner_image + filename;
                        files.push(filename);
                    }
                }



                if (data.deleteimage) {
                    fs.readdirSync(imageFolder).forEach(file => {
                        image_list.push(file)
                    });
                    var banner_images = image_list;
                    if (Array.isArray(data.deleteimage)) {
                        var image_array = (data.deleteimage).concat();
                    } else {
                        var image_array = []
                        image_array.push(data.deleteimage)
                    }
                    for (var i = 0; i < image_array.length; i++) {
                        console.log("image_array[i] = ", image_array[i])

                        var status = banner_images.includes(image_array[i].name)
                        console.log("status = ", status)
                        if (status == true) {
                            await fs.unlink(config.banner_image_path + image_array[i].name);
                        }

                    }
                }
            }

            fs.readdirSync(imageFolder).forEach(file => {
                newimage = config.get_banner_image + file
                image_list.push(newimage)
            });

            callback({
                "statuscode": 200,
                "msg": "banner images updated",
                "banner_images": image_list
            })


        }


        process.nextTick(image_data);
    },



    get_mla_profile: function (data, callback) {
        profile_data = function () {
            if (data.mla_profile_id) {
                var db = app.db;
                const mlaprofile = db.collection("mla_profile");
                let query = mlaprofile.where('mla_profile_id', '==', data.mla_profile_id).get()
                    .then(snapshot => {
                        if (snapshot.empty) {
                            callback({
                                statuscode: 404,
                                msg: "mla_profile_id not found"
                            });
                        } else {
                            snapshot.forEach(async doc => {
                                docdata = doc.data();
                                callback({
                                    statuscode: 200,
                                    msg: "Complaint details",
                                    data: docdata
                                });

                            });
                        }
                    })
            } else {
                callback({
                    statuscode: 404,
                    msg: "mla_profile_id not found"
                });
            }
        }
        process.nextTick(profile_data);
    },
}
