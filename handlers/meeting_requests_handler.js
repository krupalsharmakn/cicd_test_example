var app = require("../app");
var async = require('async');
var uuidV4 = require("../utils/utils");
var config = require("../config");
var email = require("../utils/mailer");
var crypto = require('crypto');
var configuration = require("../config");
var utils = require('../utils/utils');
var nodeMailer = require('nodemailer');
var FCM = require('fcm-push');
var fcm = new FCM(config.FCM_KEY);

module.exports = {

    new_meeting_request: function (data, callback) {
        request_data = function () {
            var unique_id = uuidV4.getUniqueId();
            var meeting_request_id = uuidV4.getUniqueId();
            var db = app.db;
            const meetingRequest = db.collection("meeting_requests");
            let query = meetingRequest.where('meeting_request_id', '==', meeting_request_id).get()
                .then(snapshot => {
                    if (snapshot.empty) {
                        if (data.requester_id) {
                            const getuser = db.collection("users");
                            getuser.where('user_id', '==', data.requester_id).get()
                                .then(snapshot => {
                                    if (snapshot.empty) {
                                        callback({
                                            statuscode: 404,
                                            msg: "user-id not found"
                                        });
                                    } else {
                                        snapshot.forEach(async userdata => {
                                            user_data = userdata.data();
                                            let new_request = {
                                                "meeting_request_id": meeting_request_id,
                                                "day_of_meeting": data.day_of_meeting,
                                                "time_of_meet": data.time_of_meet,
                                                // "meeting_duration": data.meeting_duration,
                                                "reason_for_meet": data.reason_for_meet,
                                                "subject": data.subject,
                                                "requester_id": data.requester_id,
                                                "requester_name": user_data.first_name + "" + user_data.last_name,
                                                "requester_contact_number": user_data.mobile_number,
                                                "requester_mail_id": user_data.email,
                                                "is_active": false,
                                                "created_at": new Date(Date.now()).toISOString(),
                                                "address": user_data.address,
                                                "gender": user_data.gender,
                                                "dob": user_data.dob,
                                                "marital_status": user_data.marital_status,
                                                "age": user_data.age,
                                                "request_category": data.request_category
                                            }
                                            let setNewUser = meetingRequest.doc().set(new_request);
                                            var msgBody = "Hello " + " " + ",<br><br> You have a new meeting request from " + user_data.name + "<br><br>REASON : " + data.reason_for_meet + "<br><br> Contact Details of the requester :<br>Name :" + user_data.name + "<br> Contact No :" + user_data.mobile_number + "<br> Mail-Id : " + user_data.email +
                                                "<br><br> Thank You :)";
                                            var mailid = config.secretory_mail_id
                                            let mailOptions = {
                                                from: '"POLITICIAN APPLICATION" <' + configuration.SENDER_EMAIL + '>', // sender address
                                                to: mailid,
                                                subject: 'New meeting request',
                                                text: "req.body.body",
                                                html: msgBody
                                            };
                                            email.sendMailNotification(mailOptions);
                                            callback({
                                                statuscode: 200,
                                                msg: "meeting request sent successfully",
                                                data: new_request
                                            });
                                        })
                                    }
                                })
                        } else {
                            callback({
                                statuscode: 404,
                                msg: "user-id not found"
                            });
                        }
                    } else {
                        callback({
                            statuscode: 304,
                            msg: "meeting Id exists"
                        });
                    }
                })
        }
        process.nextTick(request_data);
    },

    update_meeting_request: function (data, callback) {
        request_data = function () {
            if (data.meeting_request_id) {
                var db = app.db;
                const meetingRequest = db.collection("meeting_requests");
                let query = meetingRequest.where('meeting_request_id', '==', data.meeting_request_id).get()
                    .then(snapshot => {
                        if (snapshot.empty) {
                            callback({
                                statuscode: 404,
                                msg: "meeting request Id not found"
                            });
                        } else {
                            snapshot.forEach(doc => {
                                docdata = doc.data();
                                if (data.day_of_meeting !== undefined && data.day_of_meeting !== "") {
                                    docdata.day_of_meeting = data.day_of_meeting;
                                }
                                if (data.time_of_meet !== undefined && data.time_of_meet !== "") {
                                    docdata.time_of_meet = data.time_of_meet;
                                }
                                if (data.meeting_duration !== undefined && data.meeting_duration !== "") {
                                    docdata.meeting_duration = data.meeting_duration;
                                }
                                if (data.reason_for_meet !== undefined && data.reason_for_meet !== "") {
                                    docdata.reason_for_meet = data.reason_for_meet;
                                }
                                if (data.requester_name !== undefined && data.requester_name !== "") {
                                    docdata.requester_name = data.requester_name;
                                }
                                if (data.requester_contact_number !== undefined && data.requester_contact_number !== "") {
                                    docdata.requester_contact_number = data.requester_contact_number;
                                }
                                if (data.requester_mail_id !== undefined && data.requester_mail_id !== "") {
                                    docdata.requester_mail_id = data.requester_mail_id;
                                }
                                meetingRequest.doc(doc.id).update(docdata);
                                callback({
                                    statuscode: 200,
                                    msg: "meeting request updated successfully",
                                    data: docdata
                                });

                            });
                        }
                    })
            } else {
                callback({
                    statuscode: 404,
                    msg: "meeting request Id not found"
                });
            }
        }
        process.nextTick(request_data);
    },

    deactivate_meeting_request: function (data, callback) {
        request_data = function () {
            if (data.meeting_request_id) {
                isActive = data.is_active;
                var db = app.db;
                const meetingRequest = db.collection("meeting_requests");
                let query = meetingRequest.where('meeting_request_id', '==', data.meeting_request_id).get()
                    .then(snapshot => {
                        if (snapshot.empty) {
                            callback({
                                statuscode: 404,
                                msg: "meeting request Id not found"
                            });
                        } else {
                            var msgBody_secretory;

                            var mailid = config.secretory_mail_id
                            snapshot.forEach(doc => {
                                docdata = doc.data();
                                if (isActive == false) {
                                    docdata.is_active = false
                                    msgBody_secretory = "Hello " + " " + ",<br><br> You have rejected the meeting request requested by " + docdata.requester_mail_id + "<br><br> Thank You :)";
                                }
                                if (isActive == true) {
                                    docdata.is_active = true
                                    msgBody_secretory = "Hello " + " " + ",<br><br> You have accepted the meeting request requested by " + docdata.requester_mail_id + "<br><br> Thank You :)";
                                }
                                const getuser = db.collection("users");
                                getuser.where('user_id', '==', docdata.requester_id).get()
                                    .then(snapshot => {
                                        if (snapshot.empty) {
                                            meetingRequest.doc(doc.id).update(docdata);
                                            callback({
                                                statuscode: 200,
                                                msg: "meeting request updated"
                                            });
                                        } else {
                                            meetingRequest.doc(doc.id).update(docdata);
                                            docdata = JSON.parse(JSON.stringify(docdata))
                                            docdata.category = "meeting_request";
                                            snapshot.forEach(async doc => {
                                                UserData = doc.data();
                                                if (isActive == true) {
                                                    var message = {};
                                                    message.to = UserData.fcmtoken
                                                    message.collapse_key = "Personal Diary";
                                                    var notification = {};
                                                    notification.title = "Meeting Request";
                                                    notification.body = "Your meeting request is accepted";
                                                    message.notification = notification;
                                                    var data = {};
                                                    data = docdata;
                                                    message.data = data;
                                                    fcm.send(message, function (err, messageId) {
                                                        if (messageId) {
                                                            console.log("message is sent");
                                                        }
                                                    });
                                                }
                                                if (isActive == false) {
                                                    var message = {};
                                                    message.to = UserData.fcmtoken
                                                    message.collapse_key = "Personal Diary";
                                                    var notification = {};
                                                    notification.title = "Meeting Request";
                                                    notification.body = "Your meeting request is denied";
                                                    message.notification = notification;
                                                    var data = {};
                                                    data = docdata;
                                                    message.data = data;
                                                    fcm.send(message, function (err, messageId) {

                                                        if (messageId) {
                                                            console.log("message is sent");
                                                        }
                                                    });
                                                }
                                                callback({
                                                    statuscode: 200,
                                                    msg: "User details",
                                                    data: docdata
                                                });

                                            });
                                        }
                                    })
                                callback({
                                    statuscode: 200,
                                    msg: "meeting request deactivated successfully",
                                    data: docdata
                                });
                            });
                        }
                    });
            } else {
                callback({
                    statuscode: 404,
                    msg: "meeting request Id not found"
                });
            }
        }
        process.nextTick(request_data);
    },

    get_meeting_request: function (data, callback) {
        request_data = function () {
            if (data.meeting_request_id) {
                var db = app.db;
                const meetingrequest = db.collection("meeting_requests");
                let query = meetingrequest.where('meeting_request_id', '==', data.meeting_request_id).get()
                    .then(snapshot => {
                        if (snapshot.empty) {
                            callback({
                                statuscode: 404,
                                msg: "meeting_request_id not found"
                            });
                        } else {
                            snapshot.forEach(async doc => {
                                docdata = doc.data();
                                callback({
                                    statuscode: 200,
                                    msg: "meeting request details",
                                    data: docdata
                                });

                            });
                        }
                    });
            } else {
                callback({
                    statuscode: 404,
                    msg: "meeting request Id not found"
                });
            }
        }
        process.nextTick(request_data);
    },

    meeting_filter: function (data, callback) {
        filter_function = function () {
            var db = app.db;
            var meetingfilter = db.collection("meeting_requests");
            var query;
            if (data.is_active == true || data.is_active == false) {
                meetingfilter = meetingfilter.where("is_active", "==", data.is_active);
            }
            if (data.meeting_start_date && data.meeting_end_date) {
                meetingfilter = meetingfilter.where("day_of_meeting", ">=", data.meeting_start_date).where("day_of_meeting", "<=", data.meeting_end_date);
            }

            //both combinations doesnot work individual works
            if (data.created_start_date && data.created_end_date) {
                meetingfilter = meetingfilter.where("created_at", ">=", data.created_start_date).where("created_at", "<=", data.created_end_date);
            }
            meetingfilter.get()
                .then(snapshot => {
                    if (snapshot.empty) {
                        callback({
                            statuscode: 404,
                            msg: "meeting  not found"
                        });
                    } else {
                        var meeting_list = [];
                        snapshot.forEach(async doc => {
                            docdata = doc.data();
                            meeting_list.push(docdata)

                        });
                        callback({
                            statuscode: 200,
                            msg: "Meeting details",
                            data: meeting_list
                        });
                    }
                })

        }
        process.nextTick(filter_function);

    },

    list_meetings_by_user_id: function (data, callback) {
        request_data = function () {
            if (data.requester_id) {
                var db = app.db;
                const meetingrequest = db.collection("meeting_requests");
                var meeting_list = [];
                let query = meetingrequest.where('requester_id', '==', data.requester_id).get()
                    .then(snapshot => {
                        if (snapshot.empty) {
                            callback({
                                statuscode: 404,
                                msg: "requester_id not found"
                            });
                        } else {
                            snapshot.forEach(async doc => {
                                docdata = doc.data();
                                await meeting_list.push(docdata);


                            });
                            callback({
                                statuscode: 200,
                                msg: "Meeting requests list",
                                data: meeting_list
                            });
                        }
                    })
            } else {
                callback({
                    statuscode: 404,
                    msg: "request Id not found"
                });
            }
        }
        process.nextTick(request_data);
    },

    list_meeting_requests: function (callback) {
        Meeting_requestsData = function () {
            var db = app.db;
            const meeting_requests_list = db.collection("meeting_requests");
            let query = meeting_requests_list.get()
                .then(snapshot => {
                    if (snapshot.empty) {
                        callback({
                            statuscode: 404,
                            msg: "Meeting requests not found"
                        });
                    } else {
                        var meeting_requests_list = []
                        snapshot.forEach(async doc => {
                            docdata = doc.data();
                            meeting_requests_list.push(docdata)

                        });
                        callback({
                            statuscode: 200,
                            msg: "Meeting_requests List",
                            data: meeting_requests_list
                        });
                    }
                })
        }
        process.nextTick(Meeting_requestsData);
    }


}
