var uuidV4 = require("../utils/utils");
var async = require('async');
var config = require("../config");
var app = require("../app");
var fs = require('fs');

module.exports = {
    add_achievement: function (data, eventpath, callback) {
        achievement_data = function () {
            var achievement_id = uuidV4.getUniqueId();

            var db = app.db;
            const addachievement = db.collection("achievements");
            let query = addachievement.where('achievement_id', '==', achievement_id).get()
                .then(async snapshot => {
                    if (snapshot.empty) {
                        var reports;
                        var files = [];
                        if (eventpath == null) {
                            files = [];
                        } else {
                            if (Array.isArray(eventpath.image)) {
                                var fileArray = (eventpath.image).concat();
                            } else {
                                var fileArray = []
                                fileArray.push(eventpath.image)
                            }
                        }
                        if (eventpath != null) {
                            for (var i = 0; i < fileArray.length; i++) {
                               let sampleFile = fileArray[i];
                                var date = new Date();
                                var d = date.getTime();
                                var fileExtension = sampleFile.name.substr((sampleFile.name.lastIndexOf('.') + 1));
                                var unique_id = uuidV4.getUniqueId();
                                var filename = d + unique_id + "." + fileExtension;
                                await sampleFile.mv(config.image_path + filename);
                                var imagepath = config.get_image_path +filename;
                                files.push(imagepath);
                            }
                        } else {
                            files = []
                        }
                        let add_achievement = {
                            "achievement_id": achievement_id,
                            "achievement_title": data.achievement_title,
                            "achievement_short_description": data.achievement_short_description,
                            "achievement_description": data.achievement_description,
                            "achievement_images": files,
                            "achievement_duration": data.achievement_duration,
                            "is_active": true,
                            "created_at": new Date(Date.now()).toISOString()
                        }
                        addachievement.doc().set(add_achievement);
                        callback({
                            statuscode: 200,
                            msg: "achievement added successfully",
                            data: add_achievement
                        });
                    } else {

                        callback({
                            statuscode: 304,
                            msg: "achievement_id exists",
                            data: doc.data()
                        });

                    }
                })
        }
        process.nextTick(achievement_data);
    },

    get_achievement: function (data, callback) {
        achievement_data = function () {
            if (data.achievement_id) {
                var db = app.db;
                const getachievement = db.collection("achievements");
                let query = getachievement.where('achievement_id', '==', data.achievement_id).get()
                    .then(snapshot => {
                        if (snapshot.empty) {
                            callback({
                                statuscode: 404,
                                msg: "achievement_id not found"
                            });
                        } else {
                            snapshot.forEach(async doc => {
                                docdata = doc.data();

                                callback({
                                    statuscode: 200,
                                    msg: "achievement details",
                                    data: docdata
                                });
                            });
                        }
                    })
            } else {
                callback({
                    statuscode: 404,
                    msg: "achievement_id not found"
                });
            }
        }
        process.nextTick(achievement_data);
    },

    edit_achievement: function (data, eventpath, callback) {
        achievement_data = function () {
            if (data.achievement_id) {
                var db = app.db;
                const editachievement = db.collection("achievements");
                let query = editachievement.where('achievement_id', '==', data.achievement_id).get()
                    .then(snapshot => {
                        if (snapshot.empty) {
                            callback({
                                statuscode: 404,
                                msg: "achievement_id not found"
                            });
                        } else {
                            snapshot.forEach(async doc => {
                                docdata = doc.data();
                                var old_images = docdata.achievement_images;
                                var reports;
                                var files = [];
                                if (eventpath) {
                                    if (Array.isArray(eventpath.image)) {
                                        var fileArray = (eventpath.image).concat();
                                    } else {
                                        var fileArray = []
                                        fileArray.push(eventpath.image)
                                    }
                                    for (var i = 0; i < fileArray.length; i++) {
                                        let sampleFile = fileArray[i];
                                var date = new Date();
                                var d = date.getTime();
                                var fileExtension = sampleFile.name.substr((sampleFile.name.lastIndexOf('.') + 1));
                                var unique_id = uuidV4.getUniqueId();
                                var filename = d + unique_id + "." + fileExtension;
                                await sampleFile.mv(config.image_path + filename);
                                var imagepath = config.get_image_path +filename;

                                    files.push(imagepath);
                                        if (fileArray.length - 1 == i) {
                                            docdata.achievement_images = files;
                                            for (var i = 0; i < old_images.length; i++) {
                                                var oldimage = old_images[i].split('/');
                                                var last = oldimage[oldimage.length - 1]
                                 fs.unlink(config.image_path+last);
                                            }
                                        }
                                    }
                                } else {
                                    docdata.achievement_images = old_images;
                                }
                                if (data.achievement_title !== undefined && data.achievement_title !== "") {
                                    docdata.achievement_title = data.achievement_title;
                                }
                                if (data.achievement_short_description !== undefined && data.achievement_short_description !== "") {
                                    docdata.achievement_short_description = data.achievement_short_description;
                                }
                                if (data.achievement_description !== undefined && data.achievement_description !== "") {
                                    docdata.achievement_description = data.achievement_description;
                                }
                                if (data.achievement_duration !== undefined && data.achievement_duration !== "") {
                                    docdata.achievement_duration = data.achievement_duration;
                                }
                                editachievement.doc(doc.id).update(docdata);
                                callback({
                                    statuscode: 200,
                                    msg: "achievement details updated successfully",
                                    data: docdata
                                });
                            });
                        }
                    })
            } else {
                callback({
                    statuscode: 404,
                    msg: "achievement_id not found"
                });
            }
        }
        process.nextTick(achievement_data);
    },

    deactivate_achievement: function (data, callback) {
        achievement_data = function () {
            if (data.achievement_id) {
                var db = app.db;
                const deleteachievement = db.collection("achievements");
                deleteachievement.where('achievement_id', '==', data.achievement_id).get()
                    .then(snapshot => {
                        if (snapshot.empty) {
                            callback({
                                statuscode: 404,
                                msg: "achievement_id not found"
                            });
                        } else {
                            snapshot.forEach(doc => {
                                docdata = doc.data();
                                if (data.is_active == true) {
                                    docdata.is_active = true
                                }
                                if (data.is_active == false) {
                                    docdata.is_active = false
                                }
                                deleteachievement.doc(doc.id).update(docdata);
                                callback({
                                    statuscode: 200,
                                    msg: " achievement deactivated successfully",
                                    data: docdata
                                });
                            });
                        }
                    })
            } else {
                callback({
                    statuscode: 404,
                    msg: "achievement_id not found"
                });
            }
        }
        process.nextTick(achievement_data);
    },
}