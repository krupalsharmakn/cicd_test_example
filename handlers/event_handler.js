var uuidV4 = require("../utils/utils");
var async = require('async');
var config = require("../config");
var app = require("../app");
var FCM = require('fcm-push');
var fcm = new FCM(config.FCM_KEY);
var fs = require('fs');
module.exports = {
    add_event: function (data, eventpath, callback) {
        event_function = function () {
            var event_id = uuidV4.getUniqueId();
            var db = app.db;
            const addevent = db.collection("event");
            let datavalue = addevent.where('event_id', '==', event_id).get()
                .then(async eventdata => {
                    if (eventdata.empty) {
                        var files = [];
                        if (eventpath == null) {
                            files = [];
                        } else {
                            if (Array.isArray(eventpath.image)) {
                                var fileArray = (eventpath.image).concat();
                            } else {
                                var fileArray = []
                                fileArray.push(eventpath.image)
                            }
                        }
                        if (eventpath != null) {
                            for (var i = 0; i < fileArray.length; i++) {
                                let sampleFile = fileArray[i];
                                var date = new Date();
                                var d = date.getTime();
                                var fileExtension = sampleFile.name.substr((sampleFile.name.lastIndexOf('.') + 1));
                                var unique_id = uuidV4.getUniqueId();
                                var filename = d + unique_id + "." + fileExtension;
                                await sampleFile.mv(config.image_path + filename);
                                var imagepath = config.get_image_path + filename;
                                files.push(imagepath);
                            }
                        } else {
                            files = []
                        } 
                        let add_event = {
                            "event_id": event_id,
                            "event_title": data.event_title,
                            "event_description": data.event_description,
                            "event_start_date": data.event_start_date,
                            "event_end_date": data.event_end_date,
                            "is_active": true,
                            "event_images": files,
                            "contact_person_num": data.contact_person_num,
                            "contact_person_name": data.contact_person_name,
                            "created_at": new Date(Date.now()).toISOString(),
                            "district": data.district,
                            "taluk": data.taluk
                            //  "updated_at": new Date(Date.now()).toISOString(),
                        }
                        add_event.category = "event";

                        let dataevent = addevent.doc().set(add_event);
                        add_event = JSON.parse(JSON.stringify(add_event))

                        const getusers = db.collection("users");
                        getusers.get()
                            .then(snapshot => {
                                if (snapshot.empty) {
                                    callback({
                                        statuscode: 200,
                                        msg: "event added successfully",
                                        data: add_event
                                    });
                                } else {
                                    var user_list = []
                                    snapshot.forEach(async doc => {
                                        UserData = doc.data();
                                        user_list.push(UserData)
                                    });
                                    for (var i = 0; i < user_list.length; i++) {
                                        var message = {};
                                        message.to = user_list[i].fcmtoken;
                                        message.collapse_key = "Personal Diary";
                                        var notification = {};
                                        notification.title = "Event added";
                                        notification.body = add_event.event_title;
                                        message.notification = notification;
                                        var data = {};
                                        data = add_event;
                                        message.data = data;
                                        message = JSON.stringify(message)
                                        message = JSON.parse(message)

                                        console.log("message = ", message);
                                        fcm.send(message, function (err, messageId) {

                                            if (messageId) {
                                                console.log("message is sent");
                                            }
                                        });
                                        if (i == user_list.length - 1) {
                                            callback({
                                                statuscode: 200,
                                                msg: "event added successfully",
                                                data: add_event
                                            });
                                        }
                                    }

                                }
                            })

                    } else {
                        callback({
                            statuscode: 304,
                            msg: "event_id exists",
                            data: doc.data()
                        });
                    }

                })

        }
        process.nextTick(event_function);

    },

    get_event: function (data, callback) {
        getevent_data = function () {
            if (data.event_id) {
                var db = app.db;
                const getevent = db.collection("event");
                var eventdata = getevent.where('event_id', '==', data.event_id).get()
                    .then(event_get_data => {
                        if (event_get_data.empty) {
                            callback({
                                statuscode: 404,
                                msg: "event id not found",
                            })
                        } else {
                            event_get_data.forEach(async doc => {
                                callback({
                                    statuscode: 200,
                                    msg: "event data",
                                    data: doc.data()
                                })
                            })
                        }
                    })
            } else {
                callback({
                    statuscode: 404,
                    msg: "event id not found",
                })
            }

        }
        process.nextTick(getevent_data);

    },

    deactivate_event: function (data, callback) {
        deleteevent = function () {
            if (data.event_id) {
                var db = app.db;
                const getevent = db.collection("event");
                getevent.where('event_id', '==', data.event_id).get()
                    .then(delete_event_data => {
                        if (delete_event_data.empty) {
                            callback({
                                statuscode: 404,
                                msg: "event id not found",
                            })
                        } else {
                            delete_event_data.forEach(eventdata => {
                                docdata = eventdata.data();
                                if (data.is_active == true) {
                                    docdata.is_active = true;
                                } else if (data.is_active == false) {
                                    docdata.is_active = false
                                }
                                getevent.doc(eventdata.id).update(docdata);
                                callback({
                                    statuscode: 204,
                                    msg: "event status updated successfully",
                                })
                            })
                        }
                    })
            } else {
                callback({
                    statuscode: 404,
                    msg: "event id not found",
                })
            }
        }
        process.nextTick(deleteevent);

    },

    edit_event: function (data, eventpath, callback) {
        edit_data = function () {
            if (data.event_id) {
                var db = app.db;
                const editevent = db.collection("event");
                editevent.where('event_id', '==', data.event_id).get()
                    .then(eventdata => {
                        if (eventdata.empty) {
                            callback({
                                statuscode: 404,
                                msg: "event id not found",
                            })
                        } else {
                            eventdata.forEach(async doc => {
                                var docdata = doc.data();
                                var old_images = docdata.event_images;
                                var reports;
                                var files = [];
                                if (eventpath) {
                                    if (Array.isArray(eventpath.image)) {
                                        var fileArray = (eventpath.image).concat();
                                    } else {
                                        var fileArray = []
                                        fileArray.push(eventpath.image)
                                    }
                                    for (var i = 0; i < fileArray.length; i++) {
                                        let sampleFile = fileArray[i];
                                        var date = new Date();
                                        var d = date.getTime();
                                        var fileExtension = sampleFile.name.substr((sampleFile.name.lastIndexOf('.') + 1));
                                        var unique_id = uuidV4.getUniqueId();
                                        var filename = d + unique_id + "." + fileExtension;
                                        await sampleFile.mv(config.image_path + filename);
                                        var imagepath = config.get_image_path + filename;
                                        files.push(imagepath);
                                        if (fileArray.length - 1 == i) {
                                            docdata.event_images = files;
                                            for (var i = 0; i < old_images.length; i++) {
                                                var oldimage = old_images[i].split('/');
                                                var last = oldimage[oldimage.length - 1]
                                                fs.unlink(config.image_path + last);
                                            }
                                        }
                                    }
                                } else {
                                    docdata.event_images = old_images;
                                }
                                if (data.event_title !== undefined && data.event_title !== "") {
                                    docdata.event_title = data.event_title;
                                }
                                if (data.event_description !== undefined && data.event_description !== "") {
                                    docdata.event_description = data.event_description;
                                }
                                if (data.event_start_date !== undefined && data.event_start_date !== "") {
                                    docdata.event_start_date = data.event_start_date;
                                }
                                if (data.event_end_date !== undefined && data.event_end_date !== "") {
                                    docdata.event_end_date = data.event_end_date;
                                }
                                if (data.contact_person_num !== undefined && data.contact_person_num !== "") {
                                    docdata.contact_person_num = data.contact_person_num;
                                }
                                if (data.contact_person_name !== undefined && data.contact_person_name !== "") {
                                    docdata.contact_person_name = data.contact_person_name;
                                }
                                if (data.district !== undefined && data.district !== "") {
                                    docdata.district = data.district;
                                }
                                if (data.taluk !== undefined && data.taluk !== "") {
                                    docdata.taluk = data.taluk;
                                }
                                docdata.updated_at = new Date(Date.now()).toISOString()
                                editevent.doc(doc.id).update(docdata);
                                callback({
                                    statuscode: 200,
                                    msg: "meeting request updated successfully",
                                    data: docdata
                                });
                            })
                        }
                    })


            } else {
                callback({
                    statuscode: 404,
                    msg: "event id not found",
                })
            }
        }
        process.nextTick(edit_data);

    },
    event_filter: function (data, callback) {
        filter_function = function () {
            var db = app.db;
            var eventfilter = db.collection("event");
            var query;
            console.log("data", data);
            if (data.event_start_date && data.event_end_date) {
                eventfilter = eventfilter.where("event_start_date", ">=", data.event_start_date).get()
                    .then(snapshot => {
                        if (snapshot.empty) {
                            callback({
                                statuscode: 404,
                                msg: "event  not found"
                            });
                        }

                        else {
                            var arr = []
                            snapshot.forEach(async doc => {
                                docvalue = doc.data();
                                if (docvalue.event_end_date <= data.event_end_date) {
                                    enddata = docvalue;
                                    arr.push(enddata)
                                }
                            });

                            callback({
                                statuscode: 200,
                                msg: "event details",
                                data: arr
                            });
                        }
                    })
            }
        }
        process.nextTick(filter_function);
    },

    list_events: function (callback) {
        EventsData = function () {
            var db = app.db;
            const events_list = db.collection("event");
            let query = events_list.get()
                .then(snapshot => {
                    if (snapshot.empty) {
                        callback({
                            statuscode: 404,
                            msg: "Events not found"
                        });
                    } else {
                        var events_list = []
                        snapshot.forEach(async doc => {
                            docdata = doc.data();
                            events_list.push(docdata)

                        });
                        callback({
                            statuscode: 200,
                            msg: "Events List",
                            data: events_list
                        });
                    }
                })
        }
        process.nextTick(EventsData);
    }

}
