var app = require("../app");
var configuration = require("../config");
var randomize = require('randomatic');
var uuidV4 = require("../utils/utils");
var config = require("../config");
var fs = require('fs');
var jwt = require('jsonwebtoken');
var FCM = require('fcm-push');
var fcm = new FCM(config.FCM_KEY);
module.exports = {

    user_signup_login: function (data, callback) {
        user_data = function () {
            var random_number = randomize('0', 6);
            var user_id = uuidV4.getUniqueId();
            var unique_id = uuidV4.getUniqueId();
            if (data.mobile_number) {
                var db = app.db;
                const user = db.collection("users");
                let query = user.where('mobile_number', '==', data.mobile_number).get()
                    .then(snapshot => {
                        if (snapshot.empty) {
                            let add_user = {
                                "user_id": user_id,
                                "profile_updated": false,
                                "otp_verified": false,
                                "is_active": true,
                                "user_type": "user",
                                "mobile_number": data.mobile_number,
                                "created_at": new Date(Date.now()).toISOString(),
                                "profile_image": "",
                                "fcmtoken": ""
                            }
                            console.log(add_user);
                            user.doc().set(add_user);
                        }
                        const login_request = db.collection("login_otp_request");
                        let query2 = login_request.where('mobile_number', '==', data.mobile_number).where('otp_request_number', '==', random_number).where('is_active', '==', true).get()
                            .then(snapshot => {
                                if (snapshot.empty) {
                                    let add_login_request = {
                                        "otp_request_id": unique_id,
                                        "mobile_number": data.mobile_number,
                                        "otp_request_number": random_number,
                                        "is_active": true,
                                        "user_type": "user",
                                        "created_at": new Date(Date.now()).toISOString(),
                                        "fcmtoken": ""
                                    }
                                    console.log(add_login_request);
                                    login_request.doc().set(add_login_request);
                                    var msgBody = "Login otp request for politician application \n otp = " + random_number +
                                        " \n  Thank You .";
                                    var mobile_number = data.mobile_number;
                                    var msg91 = require("msg91")(config.msg_id, config.msg_name, "4");
                                    msg91.send(mobile_number, msgBody, function (err, response) { });
                                    callback({
                                        statuscode: 200,
                                        msg: "otp has been sent to your mobile number.Enter the otp to login."
                                    });
                                } else {
                                    callback({
                                        statuscode: 307,
                                        msg: "otp not sent ,please try again"
                                    });
                                }
                            })
                    })
            } else {
                callback({
                    statuscode: 404,
                    msg: "mobile_number not found"
                });
            }
        }
        process.nextTick(user_data);
    },

    check_otp_request: function (data, callback) {
        user_data = function () {
            if (data.mobile_number && data.otp_request_number) {
                var db = app.db;
                const login_request = db.collection("login_otp_request");
                let query2 = login_request.where('mobile_number', '==', data.mobile_number).where('otp_request_number', '==', data.otp_request_number).get()
                    .then(snapshot => {
                        if (snapshot.empty) {
                            callback({
                                statuscode: 404,
                                msg: "not valid otp"
                            })
                        } else {
                            snapshot.forEach(async doc => {
                                docdata = doc.data();
                                docdata.updated_at = new Date(Date.now()).toISOString();
                                docdata.is_active = false;
                                login_request.doc(doc.id).update(docdata);
                                const user = db.collection("users");
                                let query = user.where('mobile_number', '==', data.mobile_number).get()
                                    .then(snapshot => {
                                        if (snapshot.empty) { } else {
                                            snapshot.forEach(async doc => {
                                                var jwt_object = {};
                                                jwt_object.mobile_number = doc.mobile_number;
                                                jwt_object.user_id = doc.user_id;
                                                var jwt_token = jwt.sign({
                                                    data: jwt_object
                                                }, config.TOKEN_SECRET);
                                                docdata = doc.data();
                                                console.log("+++++++++", docdata);
                                                docdata.otp_verified = true;
                                                docdata.fcmtoken = data.fcmtoken;
                                                docdata.jwt_token = jwt_token;
                                                //docdata.user_type = user_type;

                                                user.doc(doc.id).update(docdata);
                                                console.log("docdata = ", docdata)


                                                callback({
                                                    statuscode: 200,
                                                    msg: "otp matched",
                                                    data: docdata
                                                });
                                            });
                                        }
                                    })
                            });
                        }
                    })
            } else {
                callback({
                    statuscode: 404,
                    msg: "enter both mobile number and otp"
                })
            }
        }
        process.nextTick(user_data);
    },

    delete_user: function (data, callback) {
        user_data = function () {
            if (data.user_id) {
                var db = app.db;
                const deleteuser = db.collection("users");
                let query = deleteuser.where('user_id', '==', data.user_id).get()
                    .then(snapshot => {
                        if (snapshot.empty) {
                            callback({
                                statuscode: 404,
                                msg: "user_id not found"
                            });
                        } else {
                            snapshot.forEach(doc => {
                                docdata = doc.data();
                                if (data.is_active == true) {
                                    docdata.is_active = true
                                }
                                if (data.is_active == false) {
                                    docdata.is_active = false
                                }
                                deleteuser.doc(doc.id).update(docdata);
                                callback({
                                    statuscode: 200,
                                    msg: "meeting request deactivated successfully",
                                    data: docdata
                                });
                            });
                        }
                    })
            } else {
                callback({
                    statuscode: 404,
                    msg: "user_id not found"
                });
            }
        }
        process.nextTick(user_data);
    },

    get_user: function (data, callback) {
        user_data = function () {
            if (data.user_id) {
                var db = app.db;
                const getuser = db.collection("users");
                getuser.where('user_id', '==', data.user_id).get()
                    .then(snapshot => {
                        if (snapshot.empty) {
                            callback({
                                statuscode: 404,
                                msg: "user_id not found"
                            });
                        } else {
                            snapshot.forEach(async doc => {
                                docdata = doc.data();
                                callback({
                                    statuscode: 200,
                                    msg: "User details",
                                    data: docdata
                                });

                            });
                        }
                    })
            } else {
                callback({
                    statuscode: 404,
                    msg: "user_id not found"
                });
            }
        }
        process.nextTick(user_data);
    },


    edit_user_profile: function (data, eventpath, callback) {
        profile_data = function () {
            if (data.user_id) {
                var db = app.db;
                const editprofile = db.collection("users");
                let query = editprofile.where('user_id', '==', data.user_id).get()
                    .then(snapshot => {
                        if (snapshot.empty) {
                            callback({
                                statuscode: 404,
                                msg: "mobile_number not found"
                            });
                        } else {
                            snapshot.forEach(async doc => {
                                docdata = doc.data();
                                var old_images = docdata.profile_image;
                                var files = "";
                                if (eventpath) {
                                    let sampleFile = eventpath.image;
                                    var date = new Date();
                                    var d = date.getTime();
                                    var fileExtension = sampleFile.name.substr((sampleFile.name.lastIndexOf('.') + 1));
                                    var unique_id = uuidV4.getUniqueId();
                                    var filename = d + unique_id + "." + fileExtension;
                                    await sampleFile.mv(config.image_path + filename);
                                    reports = config.get_image_path + filename;
                                    docdata.profile_image = reports;
                                    if (old_images != "" && old_images != null && old_images != undefined) {
                                        var oldimage = old_images.split('/');
                                        var last = oldimage[oldimage.length - 1]

                                        fs.unlink(config.image_path + last);
                                    }

                                } else {
                                    docdata.profile_image = old_images;
                                }
                                if (data.first_name !== undefined && data.first_name !== "") {
                                    docdata.first_name = data.first_name;
                                }
                                if (data.last_name !== undefined && data.last_name !== "") {
                                    docdata.last_name = data.last_name;
                                }
                                if (data.father_name !== undefined && data.father_name !== "") {
                                    docdata.father_name = data.father_name;
                                }
                                if (data.address !== undefined && data.address !== "") {
                                    docdata.address = data.address;
                                }
                                if (data.pincode !== undefined && data.pincode !== "") {
                                    docdata.pincode = data.pincode;
                                }
                                if (data.email !== undefined && data.email !== "") {
                                    docdata.email = data.email;
                                }
                                if (data.gender !== undefined && data.gender !== "") {
                                    docdata.gender = data.gender;
                                }
                                if (data.age !== undefined && data.age !== "") {
                                    docdata.age = data.age;
                                }
                                if (data.dob !== undefined && data.dob !== "") {
                                    docdata.dob = data.dob;
                                }
                                if (data.marital_status !== undefined && data.marital_status !== "") {
                                    docdata.marital_status = data.marital_status;
                                }
                                if (data.blood_group !== undefined && data.blood_group !== "") {
                                    docdata.blood_group = data.blood_group;
                                }
                                if (data.ward_no !== undefined && data.ward_no !== "") {
                                    docdata.ward_no = data.ward_no;
                                }
                                if (data.district !== undefined && data.district !== "") {
                                    docdata.district = data.district;
                                }
                                if (data.taluk !== undefined && data.taluk !== "") {
                                    docdata.taluk = data.taluk;
                                }
                                docdata.updated_at = new Date(Date.now()).toISOString();
                                docdata.profile_updated = true;
                                if (docdata.email == "" || docdata.email == undefined || docdata.email == null) {
                                    callback({
                                        statuscode: 206,
                                        msg: "email not entered"
                                    });
                                } else if (docdata.first_name == "" || docdata.first_name == undefined || docdata.first_name == null) {
                                    callback({
                                        statuscode: 206,
                                        msg: "name not entered"
                                    });
                                } else {
                                    editprofile.doc(doc.id).update(docdata);
                                    callback({
                                        statuscode: 200,
                                        msg: "profile updated successfully",
                                        data: docdata
                                    });
                                }

                            });
                        }
                    })
            } else {
                callback({
                    statuscode: 404,
                    msg: "user_id not found"
                });
            }
        }
        process.nextTick(profile_data);
    },

    user_filter: function (data, callback) {
        filter_function = function () {
            var db = app.db;
            var userfilter = db.collection("users");
            var query;
            if (data.is_active) {
                var dataactive = JSON.stringify(data.is_active)
                userfilter = userfilter.where("is_active", "==", dataactive);
            }
            if (data.age) {
                userfilter = userfilter.where("age", "==", data.age);
            }
            if (data.marital_status) {
                userfilter = userfilter.where("marital_status", "==", data.marital_status);

            }

            if (data.pincode) {
                userfilter = userfilter.where("pincode", "==", data.pincode);
            }
            if (data.blood_group) {
                userfilter = userfilter.where("blood_group", "==", data.blood_group);

            }
            if (data.gender) {
                userfilter = userfilter.where("gender", "==", data.gender);
            }
            if (data.ward_no) {
                userfilter = userfilter.where("ward_no", "==", data.ward_no);
            }

            userfilter.get()
                .then(snapshot => {
                    if (snapshot.empty) {
                        callback({
                            statuscode: 404,
                            msg: "user_id not found"
                        });
                    } else {
                        var user_list = [];
                        snapshot.forEach(async doc => {
                            docdata = doc.data();
                            user_list.push(docdata)

                        });
                        callback({
                            statuscode: 200,
                            msg: "User details",
                            data: user_list
                        });
                    }
                })
        }
        process.nextTick(filter_function)
    },

    send_message: function (data, callback) {
        user_data = function () {
            if (data) {
                var db = app.db;
                if (data.to == "all") {
                    var newdata = data;
                    const getusers = db.collection("users");
                    getusers.get()
                        .then(snapshot => {
                            if (snapshot.empty) {
                                callback({
                                    statuscode: 500,
                                    msg: "database err,no users found"
                                });
                            } else {
                                var user_list = []
                                snapshot.forEach(async doc => {
                                    UserData = doc.data();
                                    user_list.push(UserData)
                                });
                                for (var i = 0; i < user_list.length; i++) {
                                    var message = {};
                                    message.to = user_list[i].fcmtoken;
                                    message.collapse_key = "Personal Diary";
                                    var notification = {};
                                    console.log("data.notification_title = ", newdata.notification_title)

                                    notification.title = JSON.stringify(newdata.notification_title);
                                    notification.body = newdata.notification_message;
                                    message.notification = notification;
                                    var data = {};
                                    data.category = "notification";
                                    data.notification_message = newdata.notification_message;
                                    message.data = data;
                                    message = JSON.stringify(message)
                                    message = JSON.parse(message)
                                    console.log("message = ", message);
                                    fcm.send(message, function (err, messageId) {
                                        if (messageId) {
                                            console.log("message is sent");
                                        }
                                    });
                                    if (i == user_list.length - 1) {
                                        callback({
                                            statuscode: 200,
                                            msg: "notification sent "
                                        });
                                    }
                                }
                            }
                        })
                } else if (data.to == "individual") {
                    var newdata = data;
                    if (newdata.user_id) {
                        const getuser = db.collection("users");
                        getuser.where('user_id', '==', newdata.user_id).get()
                            .then(snapshot => {
                                if (snapshot.empty) {
                                    callback({
                                        statuscode: 404,
                                        msg: "user_id not found"
                                    });
                                } else {
                                    snapshot.forEach(async doc => {
                                        docdata = doc.data();
                                        var message = {};
                                        message.to = docdata.fcmtoken;
                                        message.collapse_key = "Personal Diary";
                                        var notification = {};
                                        notification.title = newdata.notification_title;
                                        notification.body = newdata.notification_message;
                                        message.notification = notification;
                                        var data = {};
                                        data.category = "notification";
                                        data.notification_message = newdata.notification_message;
                                        message.data = data;
                                        message = JSON.stringify(message)
                                        message = JSON.parse(message)
                                        console.log("message = ", message);
                                        fcm.send(message, function (err, messageId) {
                                            if (messageId) {
                                                console.log("message is sent");
                                            }
                                        });
                                        callback({
                                            statuscode: 200,
                                            msg: "Notification sent"
                                        });
                                    });
                                }
                            })
                    } else {
                        callback({
                            statuscode: 404,
                            msg: "user_id not found"
                        });
                    }
                }
            } else {
                callback({
                    statuscode: 404,
                    msg: "no data found"
                });
            }
        }
        process.nextTick(user_data);
    },

    birthday_message: function (callback) {
        user_data = function () {
            var today = new Date();
            var date = (today.getMonth() + 1) + '-' + today.getDate();
            var db = app.db;
            const getusers = db.collection("users");
            getusers.get()
                .then(snapshot => {
                    if (snapshot.empty) {
                        callback({
                            statuscode: 500,
                            msg: "database err,no users found"
                        });
                    } else {
                        var user_list = []
                        snapshot.forEach(async doc => {
                            UserData = doc.data();
                            user_list.push(UserData)
                        });
                        for (var i = 0; i < user_list.length; i++) {
                            var DOB = user_list[i].dob;
                            var myDate = new Date(DOB);
                            var DOB_DATE_MONTH = (myDate.getMonth() + 1) + '-' + myDate.getDate();
                            var DOB_DATE_MONTH_OBJ = DOB_DATE_MONTH.toString()
                            var DATE_OBJ = date.toString()
                            if (DOB_DATE_MONTH_OBJ == DATE_OBJ) {
                                console.log("birthday_boy_fcm", user_list[i].fcmtoken)
                                console.log("birthday_boy", DOB_DATE_MONTH_OBJ == DATE_OBJ)
                                var message = {};
                                message.to = user_list[i].fcmtoken;
                                message.collapse_key = "Personal Diary";
                                var notification = {};
                                notification.title = "Happy Birthday";
                                notification.body = "Sending you smiles for every moment of your special day…Have a wonderful time and a very happy birthday";
                                message.notification = notification;
                                var data = {};
                                data.category = "notification";
                                data.notification_message = "Sending you smiles for every moment of your special day…Have a wonderful time and a very happy birthday";
                                message.data = data;
                                message = JSON.stringify(message)
                                message = JSON.parse(message)
                                //console.log("message = ", message);
                                fcm.send(message, function (err, messageId) {
                                    if (messageId) {
                                        console.log("message is sent");
                                    }
                                });
                                if (i == user_list.length - 1) {
                                    callback({
                                        statuscode: 200,
                                        msg: "notification sent "
                                    });
                                }
                            }
                        }
                    }
                })
        }
        process.nextTick(user_data);
    },

    list_users: function (callback) {
        UserData = function () {
            var db = app.db;
            const user_list = db.collection("users");
            let query = user_list.get()
                .then(snapshot => {
                    if (snapshot.empty) {
                        callback({
                            statuscode: 404,
                            msg: "User not found"
                        });
                    } else {
                        var user_list = []
                        snapshot.forEach(async doc => {
                            docdata = doc.data();
                            user_list.push(docdata)

                        });
                        callback({
                            statuscode: 200,
                            msg: "User List",
                            data: user_list
                        });
                    }
                })
        }
        process.nextTick(UserData);
    }
}
