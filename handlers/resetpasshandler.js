var Reset = require("../models/user");
var bCrypt = require('bcrypt-nodejs');
module.exports = {

    resetpassword_data: function (data, callback) {
        resetpassworddata = function () {

            var encpassword = createHash(data.password);
            Reset.findOne({
                "resetpassword": data.resetpassword
            }, function (err, UserResult) {
                if (UserResult) {
                    UserResult.password = encpassword;
                    UserResult.save(function (err, usersave) {
                        if (err) {
                            console.log("Error in updating User " + err);
                            callback({
                                "statuscode": "500",
                                "msg": ""
                            });
                        } else if (usersave) {
                            callback({
                                "msg": "reset success",
                                "statuscode": "204"
                            });
                        } else {
                            callback({
                                "msg": "reset failure",
                                "statuscode": "404"
                            });
                        }

                    });
                }


            });


        };
        process.nextTick(resetpassworddata);
    }
};
var createHash = function (password) {
    return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
};