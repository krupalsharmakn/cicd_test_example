var uuidV4 = require("../utils/utils");
var async = require('async');
var config = require("../config");
var app = require("../app");
var fs = require('fs');
var FCM = require('fcm-push');
var fcm = new FCM(config.FCM_KEY);

module.exports = {
    add_news: function (data, eventpath, callback) {
        newsData = function () {
            var news_id = uuidV4.getUniqueId();
            var db = app.db;
            const addnews = db.collection("news");
            let query = addnews.where('news_id', '==', news_id).get()
                .then(async snapshot => {
                    if (snapshot.empty) {
                        var reports;
                        var files = [];
                        if (eventpath == null) {
                            files = [];
                        } else {
                            if (Array.isArray(eventpath.image)) {
                                var fileArray = (eventpath.image).concat();
                            } else {
                                var fileArray = []
                                fileArray.push(eventpath.image)
                            }
                        }
                        if (eventpath != null) {
                            for (var i = 0; i < fileArray.length; i++) {
                                let sampleFile = fileArray[i];
                                var date = new Date();
                                var d = date.getTime();
                                var fileExtension = sampleFile.name.substr((sampleFile.name.lastIndexOf('.') + 1));
                                var unique_id = uuidV4.getUniqueId();
                                var filename = d + unique_id + "." + fileExtension;
                                await sampleFile.mv(config.image_path + filename);
                                var imagepath = config.get_image_path + filename;
                                files.push(imagepath);
                            }
                        } else {
                            files = []
                        }
                        let add_news = {
                            "news_id": news_id,
                            "news_title": data.news_title,
                            "news_description": data.news_description,
                            "news_images": files,
                            "news_reported_on": data.news_reported_on,
                            "is_active": true,
                            "created_at": new Date(Date.now()).toISOString(),
                            "news_category": data.news_category
                        }
                        let setNews = addnews.doc().set(add_news);
                        add_news = JSON.parse(JSON.stringify(add_news))

                        const getusers = db.collection("users");
                        getusers.get()
                            .then(snapshot => {
                                if (snapshot.empty) {
                                    callback({
                                        statuscode: 200,
                                        msg: "News added successfully",
                                        data: add_news
                                    });
                                } else {
                                    var user_list = []
                                    snapshot.forEach(async doc => {
                                        UserData = doc.data();
                                        user_list.push(UserData)
                                    });
                                    for (var i = 0; i < user_list.length; i++) {
                                        var message = {};
                                        message.to = user_list[i].fcmtoken;
                                        message.collapse_key = "Personal Diary";
                                        var notification = {};
                                        notification.title = "News added";
                                        notification.body = add_news.news_title;
                                        message.notification = notification;
                                        var data = {};
                                        data = add_news;
                                        message.data = data;
                                        message = JSON.stringify(message)
                                        message = JSON.parse(message)

                                        console.log("message = ", message);
                                        fcm.send(message, function (err, messageId) {

                                            if (messageId) {
                                                console.log("message is sent");
                                            }
                                        });
                                        if (i == user_list.length - 1) {
                                            callback({
                                                statuscode: 200,
                                                msg: "News added successfully",
                                                data: add_news
                                            });
                                        }
                                    }

                                }
                            })

                        // callback({
                        //     statuscode: 200,
                        //     msg: "news added successfully",
                        //     data: add_news
                        // });
                    } else {
                        callback({
                            statuscode: 304,
                            msg: "news_id exists"
                        });
                    }
                })
        }
        process.nextTick(newsData);
    },

    edit_news: function (data, eventpath, callback) {
        news_data = function () {
            if (data.news_id) {
                var db = app.db;
                const editnews = db.collection("news");
                let query = editnews.where('news_id', '==', data.news_id).get()
                    .then(snapshot => {
                        if (snapshot.empty) {
                            callback({
                                statuscode: 404,
                                msg: "news_id not found"
                            });
                        } else {
                            snapshot.forEach(async doc => {
                                docdata = doc.data();
                                var old_images = docdata.news_images;
                                var reports;
                                var files = [];
                                if (eventpath) {
                                    if (Array.isArray(eventpath.image)) {
                                        var fileArray = (eventpath.image).concat();
                                    } else {
                                        fileArray.push(eventpath.image)
                                    }
                                    for (var i = 0; i < fileArray.length; i++) {
                                        let sampleFile = fileArray[i];
                                        var date = new Date();
                                        var d = date.getTime();
                                        var fileExtension = sampleFile.name.substr((sampleFile.name.lastIndexOf('.') + 1));
                                        var unique_id = uuidV4.getUniqueId();
                                        var filename = d + unique_id + "." + fileExtension;
                                        await sampleFile.mv(config.image_path + filename);
                                        var imagepath = config.get_image_path + filename;
                                        files.push(imagepath);
                                        if (fileArray.length - 1 == i) {
                                            docdata.news_images = files;
                                            for (var i = 0; i < old_images.length; i++) {
                                                var oldimage = old_images[i].split('/');
                                                var last = oldimage[oldimage.length - 1]
                                                fs.unlink(config.image_path + last);
                                            }
                                        }
                                    }
                                } else {
                                    docdata.news_images = old_images;
                                }
                                if (data.news_title !== undefined && data.news_title !== "") {
                                    docdata.news_title = data.news_title;
                                }
                                if (data.news_description !== undefined && data.news_description !== "") {
                                    docdata.news_description = data.news_description;
                                }
                                if (data.news_category !== undefined && data.news_category !== "") {
                                    docdata.news_category = data.news_category;
                                }
                                if (data.news_reported_on !== undefined && data.news_reported_on !== "") {
                                    docdata.news_reported_on = data.news_reported_on;
                                }
                                editnews.doc(doc.id).update(docdata);
                                callback({
                                    statuscode: 200,
                                    msg: "meeting request updated successfully",
                                    data: docdata
                                });

                            });
                        }
                    })
            } else {
                callback({
                    statuscode: 404,
                    msg: "news_id not found"
                });
            }


        }
        process.nextTick(news_data);
    },

    get_news: function (data, callback) {
        news_data = function () {
            if (data.news_id) {
                var db = app.db;
                const getnews = db.collection("news");
                let query = getnews.where('news_id', '==', data.news_id).get()
                    .then(snapshot => {
                        if (snapshot.empty) {
                            callback({
                                statuscode: 404,
                                msg: "news_id not found"
                            });
                        } else {
                            snapshot.forEach(async doc => {
                                docdata = doc.data();
                                callback({
                                    statuscode: 200,
                                    msg: "News details updated successfully",
                                    data: docdata
                                });
                            });
                        }
                    })
            } else {
                callback({
                    statuscode: 404,
                    msg: "news_id not found"
                });
            }
        }
        process.nextTick(news_data);
    },

    deactivate_news: function (data, callback) {
        news_data = function () {
            if (data.news_id) {
                var db = app.db;
                const deactivatenews = db.collection("news");
                deactivatenews.where('news_id', '==', data.news_id).get()
                    .then(snapshot => {
                        if (snapshot.empty) {
                            callback({
                                statuscode: 404,
                                msg: "news_id not found"
                            });
                        } else {
                            snapshot.forEach(doc => {
                                docdata = doc.data();
                                if (data.is_active == true) {
                                    docdata.is_active = true
                                }
                                if (data.is_active == false) {
                                    docdata.is_active = false
                                }
                                deactivatenews.doc(doc.id).update(docdata);
                                callback({
                                    statuscode: 200,
                                    msg: " News deactivated successfully",
                                    data: docdata
                                });
                            });
                        }
                    })
            } else {
                callback({
                    statuscode: 404,
                    msg: "news_id not found"
                });
            }
        };
        process.nextTick(news_data);
    },

    list_news_images: function (callback) {
        news_data = function () {
            let news_list = []
            let news_images = []
            var db = app.db;
            const getnews = db.collection("news");
            let query = getnews.get()
                .then(snapshot => {
                    if (snapshot.empty) {

                        callback({
                            statuscode: 404,
                            msg: "NO news"
                        });
                    } else {
                        snapshot.forEach(async doc => {
                            docdata = doc.data();
                            await news_list.push(docdata)
                            for (i = 0; i < news_list.length; i++) {
                                for (j = 0; j < 1; j++) {
                                    news_images.push(news_list[i].news_images[j])
                                }
                                if (i == news_list.length - 1) {
                                    callback({
                                        statuscode: 200,
                                        msg: "News Images",
                                        data: news_images
                                    });
                                }
                            }
                        });
                    }
                })
        }
        process.nextTick(news_data);
    },

    list_news_by_category: function (data, callback) {
        news_data = function () {
            if (data.news_category) {
                var db = app.db;
                var lower_case_category = data.news_category.toLowerCase();
                console.log("lower_case_category", lower_case_category);
                const getnews = db.collection("news");
                let query = getnews.where('news_category', '==', lower_case_category).where('is_active', '==', true).get()
                    .then(snapshot => {
                        if (snapshot.empty) {
                            callback({
                                statuscode: 404,
                                msg: "No news for this category"
                            });
                        } else {
                            var news_list = []
                            snapshot.forEach(async doc => {
                                docdata = doc.data();
                                news_list.push(docdata)

                            });
                            callback({
                                statuscode: 200,
                                msg: "News List",
                                data: news_list.slice().sort((a, b) => new Date(b.news_reported_on) - new Date(a.news_reported_on))
                            });
                        }
                    })
            } else {
                callback({
                    statuscode: 404,
                    msg: "news_category not found"
                });
            }
        }
        process.nextTick(news_data);
    },

    list_news: function (callback) {
        NewsData = function () {
            var db = app.db;
            const news_list = db.collection("news");
            let query = news_list.get()
                .then(snapshot => {
                    if (snapshot.empty) {
                        callback({
                            statuscode: 404,
                            msg: "News not found"
                        });
                    } else {
                        var news_list = []
                        snapshot.forEach(async doc => {
                            docdata = doc.data();
                            news_list.push(docdata)

                        });
                        callback({
                            statuscode: 200,
                            msg: "News List",
                            data: news_list
                        });
                    }
                })
        }
        process.nextTick(NewsData);
    }
}
