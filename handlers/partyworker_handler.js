var uuidV4 = require("../utils/utils");
var async = require('async');
var config = require("../config");
var app = require("../app");
var fs = require('fs');

module.exports = {
    add_partyworker: function (data, eventpath, callback) {
        partyworkerData = function () {
            var party_worker_id = uuidV4.getUniqueId();
            var db = app.db;
            const addpartyworker = db.collection("partyworker");
            let query = addpartyworker.where('phone_number', '==', data.phone_number).get()
                .then(async snapshot => {
                    if (snapshot.empty) {
                        var reports = "";
                        if (eventpath) {
                            let sampleFile = eventpath.image;
                            var date = new Date();
                            var d = date.getTime();
                            var fileExtension = sampleFile.name.substr((sampleFile.name.lastIndexOf('.') + 1));
                            var unique_id = uuidV4.getUniqueId();
                            var filename = d + unique_id + "." + fileExtension;
                            await sampleFile.mv(config.image_path + filename);
                            reports = config.get_image_path + filename;
                        } else {
                            reports = "";
                        }
                        let add_partyworker = {
                            "party_worker_id": party_worker_id,
                            "party_worker_type": data.party_worker_type,
                            "name": data.name,
                            "gender": data.gender,
                            "DOB": data.DOB,
                            "age": data.age,
                            "phone_number": data.phone_number,
                            "alternative_number": data.alternative_number,
                            "is_active": true,
                            "created_at": new Date(Date.now()).toISOString(),
                            "email": data.email,
                            "district": data.district,
                            "taluk": data.taluk,
                            "partyworker_images": reports,
                            "panchayath_ward": data.panchayath_ward,
                            "pin": data.pin,
                            "age_range": data.age_range
                        }
                        let setPartyworker = addpartyworker.doc().set(add_partyworker);
                        const adduser = db.collection("users");
                        let query = adduser.where('mobile_number', '==', data.phone_number).get()
                            .then(async snapshot => {
                                if (snapshot.empty) {
                                    let add_user = {
                                        "user_id": party_worker_id,
                                        "mobile_number": data.phone_number,
                                        "is_active": true,
                                        "user_type": "party_worker",
                                        "created_at": new Date(Date.now()).toISOString(),
                                        "fcmtoken": "",
                                    }
                                    let setUser = adduser.doc().set(add_user);
                                    var msgBody = "Please login to politician App";
                                    var mobile_number = data.phone_number;
                                    var msg91 = require("msg91")(config.msg_id, config.msg_name, "4");
                                    msg91.send(mobile_number, msgBody, function (err, response) { });
                                    callback({
                                        statuscode: 200,
                                        msg: "Party worker added successfully",
                                        data: add_partyworker
                                    });
                                } else {
                                    callback({
                                        statuscode: 304,
                                        msg: "party_worker  exists"
                                    });
                                }
                            })
                    } else {
                        console.log("*************")
                        callback({
                            statuscode: 304,
                            msg: "party_worker  exists"
                        });
                    }
                })
        }
        process.nextTick(partyworkerData);
    },

    edit_partyworker: function (data, eventpath, callback) {
        partyworkerData = function () {
            if (data.party_worker_id) {
                var db = app.db;
                const editpartyworker = db.collection("partyworker");
                let query = editpartyworker.where('party_worker_id', '==', data.party_worker_id).get()
                    .then(snapshot => {
                        if (snapshot.empty) {
                            callback({
                                statuscode: 404,
                                msg: "party_worker_id not found"

                            });
                        } else {
                            snapshot.forEach(async doc => {
                                docdata = doc.data();
                                var old_images = docdata.partyworker_images
                                if (eventpath) {
                                    let sampleFile = eventpath.image;
                                    var date = new Date();
                                    var d = date.getTime();
                                    var fileExtension = sampleFile.name.substr((sampleFile.name.lastIndexOf('.') + 1));
                                    var unique_id = uuidV4.getUniqueId();
                                    var filename = d + unique_id + "." + fileExtension;
                                    await sampleFile.mv(config.image_path + filename);
                                    reports = config.get_image_path + filename;
                                    docdata.partyworker_images = reports;
                                    // if (old_images != "" && old_images != null && old_images != undefined) {
                                    //     var oldimage = old_images;
                                    //     var last = oldimage;

                                    //     fs.unlink(config.image_path + last);
                                    // }

                                } else {
                                    docdata.partyworker_images = old_images;
                                }
                                if (data.party_worker_type !== undefined && data.party_worker_type !== "") {
                                    docdata.party_worker_type = data.party_worker_type;
                                }
                                if (data.name !== undefined && data.name !== "") {
                                    docdata.name = data.name;
                                }
                                if (data.gender !== undefined && data.gender !== "") {
                                    docdata.gender = data.gender;
                                }
                                if (data.DOB !== undefined && data.DOB !== "") {
                                    docdata.DOB = data.DOB;
                                }
                                if (data.age !== undefined && data.age !== "") {
                                    docdata.age = data.age;
                                }
                                if (data.phone_number !== undefined && data.phone_number !== "") {
                                    docdata.phone_number = data.phone_number;
                                }
                                if (data.alternative_number !== undefined && data.alternative_number !== "") {
                                    docdata.alternative_number = data.alternative_number;
                                }
                                if (data.email !== undefined && data.email !== "") {
                                    docdata.email = data.email;
                                }
                                if (data.district !== undefined && data.district !== "") {
                                    docdata.district = data.district;
                                }
                                if (data.taluk !== undefined && data.taluk !== "") {
                                    docdata.taluk = data.taluk;
                                }
                                if (data.panchayath_ward !== undefined && data.panchayath_ward !== "") {
                                    docdata.panchayath_ward = data.panchayath_ward;
                                }
                                if (data.pin !== undefined && data.pin !== "") {
                                    docdata.pin = data.pin;
                                }
                                if (data.age_range !== undefined && data.age_range !== "") {
                                    docdata.age_range = data.age_range;
                                }
                                editpartyworker.doc(doc.id).update(docdata);
                                console.log("Updated")
                                callback({
                                    statuscode: 200,
                                    msg: "Party worker request updated successfully",
                                    data: docdata
                                });

                            });
                        }
                    })
            } else {
                callback({
                    statuscode: 404,
                    msg: "party_worker_id not found"
                });
            }
        }
        process.nextTick(partyworkerData);
    },

    get_partyworker: function (data, callback) {
        partyworkerData = function () {
            if (data.party_worker_id) {
                var db = app.db;
                const getpartyworker = db.collection("partyworker");
                let query = getpartyworker.where('party_worker_id', '==', data.party_worker_id).get()
                    .then(snapshot => {
                        if (snapshot.empty) {
                            callback({
                                statuscode: 404,
                                msg: "party_worker_id not found"
                            });
                        } else {
                            snapshot.forEach(async doc => {
                                docdata = doc.data();
                                callback({
                                    statuscode: 200,
                                    msg: "Party worker details",
                                    data: docdata
                                });
                            });
                        }
                    })
            } else {
                callback({
                    statuscode: 404,
                    msg: "party_worker_id not found"
                });
            }
        }
        process.nextTick(partyworkerData);
    },

    deactivate_partyworker: function (data, callback) {
        partyworkerData = function () {
            if (data.party_worker_id) {
                var db = app.db;
                const deactivatepartyworker = db.collection("partyworker");
                deactivatepartyworker.where('party_worker_id', '==', data.party_worker_id).get()
                    .then(snapshot => {
                        if (snapshot.empty) {
                            callback({
                                statuscode: 404,
                                msg: "party_worker_id not found"
                            });
                        } else {
                            snapshot.forEach(doc => {
                                docdata = doc.data();
                                if (data.is_active == true) {
                                    docdata.is_active = true
                                }
                                if (data.is_active == false) {
                                    docdata.is_active = false
                                }
                                deactivatepartyworker.doc(doc.id).update(docdata);
                                console.log("Updated")
                                callback({
                                    statuscode: 200,
                                    msg: " Party worker deactivated successfully",
                                    data: docdata
                                });
                            });
                        }
                    })
            } else {
                callback({
                    statuscode: 404,
                    msg: "party_worker_id not found"
                });
            }
        };
        process.nextTick(partyworkerData);
    },

    list_partyworkers_by_workerstype: function (data, callback) {
        partyworkerData = function () {
            if (data.party_worker_type) {
                var db = app.db;
                var lower_case_category = data.party_worker_type.toLowerCase();
                console.log("lower_case_category", lower_case_category);
                const list_partyworkers_by_workerstype = db.collection("partyworker");
                let query = list_partyworkers_by_workerstype.where('party_worker_type', '==', lower_case_category).where('is_active', '==', true).get()
                    .then(snapshot => {
                        if (snapshot.empty) {
                            callback({
                                statuscode: 404,
                                msg: "No Party workers for this type"
                            });
                        } else {
                            var partyworkers_list = []
                            snapshot.forEach(async doc => {
                                docdata = doc.data();
                                partyworkers_list.push(docdata)

                            });
                            callback({
                                statuscode: 200,
                                msg: "Party worker List",
                                data: partyworkers_list
                            });
                        }
                    })
            } else {
                callback({
                    statuscode: 404,
                    msg: "Party worker type not found"
                });
            }
        }
        process.nextTick(partyworkerData);
    },

    partyworker_filter: function (data, callback) {
        partyworkerData = function () {
            var db = app.db;
            var partyworkerfilter = db.collection("partyworker");
            var partyworker_list = [];
            if (data.party_worker_type) {
                partyworkerfilter = partyworkerfilter.where("party_worker_type", "==", data.party_worker_type);
            }
            if (data.taluk) {
                partyworkerfilter = partyworkerfilter.where("taluk", "==", data.taluk);
            }
            if (data.district) {
                partyworkerfilter = partyworkerfilter.where("district", "==", data.district);
            }
            if (data.panchayath_ward) {
                partyworkerfilter = partyworkerfilter.where("panchayath_ward", "==", data.panchayath_ward);
            }
            if (data.pin) {
                partyworkerfilter = partyworkerfilter.where("pin", "==", data.pin);
            }
            if (data.age_range) {
                partyworkerfilter = partyworkerfilter.where("age_range", "==", data.age_range);
            }
            if (data.gender) {
                partyworkerfilter = partyworkerfilter.where("gender", "==", data.gender);
            }
            partyworkerfilter.get()
                .then(snapshot => {
                    if (snapshot.empty) {
                        callback({
                            statuscode: 404,
                            msg: "No Party worker found"
                        });
                    } else {
                        snapshot.forEach(async doc => {
                            docdata = doc.data();
                            await partyworker_list.push(docdata);
                        });
                        callback({
                            statuscode: 200,
                            msg: "Complaint details",
                            data: partyworker_list
                        });
                    }
                })
        }
        process.nextTick(partyworkerData);
    },

    list_partyworkers: function (callback) {
        partyworkerData = function () {
            var db = app.db;
            const list_partyworkers = db.collection("partyworker");
            let query = list_partyworkers.get()
                .then(snapshot => {
                    if (snapshot.empty) {
                        callback({
                            statuscode: 404,
                            msg: "No Party workers for this type"
                        });
                    } else {
                        var partyworkers_list = []
                        snapshot.forEach(async doc => {
                            docdata = doc.data();
                            partyworkers_list.push(docdata)

                        });
                        callback({
                            statuscode: 200,
                            msg: "Party worker List",
                            data: partyworkers_list
                        });
                    }
                })
        }
        process.nextTick(partyworkerData);
    }
}
