var uuidV4 = require("../utils/utils");
var async = require('async');
var app = require("../app");
var configuration = require("../config");
var email = require("../utils/mailer");
var AWS_S3 = require("../utils/aws-s3");
var config = require("../config");
var fs = require('fs');
var FCM = require('fcm-push');
var fcm = new FCM(config.FCM_KEY);

module.exports = {
    add_complaint: function (data, eventpath, callback) {
        complaint_data = function () {
            var handlerdata = data;
            handlerdata.location = JSON.parse(handlerdata.location);
            var arr = [];
            arr.push(handlerdata.location[0]);
            arr.push(handlerdata.location[1])
            var db = app.db;
            const addcomplaint = db.collection("complaints");
            var complaint_id = uuidV4.getUniqueId();
            let query = addcomplaint.where('complaint_id', '==', complaint_id).get()
                .then(async snapshot => {
                    if (snapshot.empty) {
                        if (handlerdata.user_id) {
                            const getuser = db.collection("users");
                            getuser.where('user_id', '==', handlerdata.user_id).get()
                                .then(snapshot => {
                                    if (snapshot.empty) {
                                        callback({
                                            statuscode: 404,
                                            msg: "user-id not found"
                                        });
                                    } else {
                                        snapshot.forEach(async userdata => {
                                            user_data = userdata.data();
                                            var reports;
                                            var files = [];
                                            if (eventpath == null) {
                                                files = [];
                                            } else {
                                                if (Array.isArray(eventpath.image)) {
                                                    var fileArray = (eventpath.image).concat();
                                                } else {
                                                    var fileArray = []
                                                    fileArray.push(eventpath.image)
                                                }
                                            }
                                            if (eventpath) {
                                                for (var i = 0; i < fileArray.length; i++) {

                                                    let sampleFile = fileArray[i];
                                                    var date = new Date();
                                                    var d = date.getTime();
                                                    var fileExtension = sampleFile.name.substr((sampleFile.name.lastIndexOf('.') + 1));
                                                    var unique_id = uuidV4.getUniqueId();
                                                    var filename = d + unique_id + "." + fileExtension;
                                                    await sampleFile.mv(config.image_path + filename);
                                                    var imagepath = config.get_image_path + filename;
                                                    files.push(imagepath);
                                                }
                                            } else {
                                                files = []
                                            }
                                            let add_complaint = {
                                                "complaint_id": complaint_id,
                                                // "complaint_title": data.complaint_title,
                                                "complaint_description": handlerdata.complaint_description,
                                                "complaint_category": handlerdata.complaint_category,
                                                "user_id": handlerdata.user_id,
                                                "user_contact_no": user_data.mobile_number,
                                                "user_mail_id": user_data.email,
                                                "complaint_images": files,
                                                "complaint_status": 0,
                                                "taluk": handlerdata.taluk,
                                                "union": handlerdata.union,
                                                "town_panchayat": handlerdata.town_panchayat,
                                                "village_panchayat": handlerdata.village_panchayat,
                                                "place_name": handlerdata.place_name,
                                                "ward_no": handlerdata.ward_no,
                                                "street_name": handlerdata.street_name,
                                                "created_at": new Date(Date.now()).toISOString(),
                                                "location": arr,
                                                "first_name": user_data.first_name,
                                                "last_name": user_data.last_name,
                                                "address": user_data.address,
                                                "gender": user_data.gender,
                                                "dob": user_data.dob,
                                                "marital_status": user_data.marital_status,
                                                "age": user_data.age
                                            }
                                            let setComplaint = addcomplaint.doc().set(add_complaint);

                                            add_complaint = JSON.parse(JSON.stringify(add_complaint))
                                            add_complaint.category = "complaint";
                                            var created_at = new Date();
                                            var day = created_at.getDate();
                                            var month = created_at.getMonth() + 1;
                                            var year = created_at.getFullYear();
                                            var date = day + '-' + month + '-' + year;
                                            add_complaint.day_of_complaint = date;
                                            var message = {};
                                            message.to = user_data.fcmtoken;
                                            message.collapse_key = "Personal Diary";
                                            var notification = {};
                                            notification.title = "New Complaint";
                                            notification.body = "New Complaint " + "  has been registerird by you .";
                                            message.notification = notification;
                                            var data = {
                                                "category_id": add_complaint.complaint_id,
                                                "category_title": "",
                                                "category_description": add_complaint.complaint_description,
                                                "category_status": add_complaint.complaint_status,
                                                "category_image": add_complaint.complaint_images,
                                                "category_created_at": add_complaint.day_of_complaint,
                                                "category_type": "grievance"
                                            };

                                            message.data = data;
                                            fcm.send(message, function (err, messageId) {
                                                if (messageId) {
                                                    console.log("message is sent");
                                                }
                                            });
                                            var msgBody_secretory = "Hello " + " " + ",<br><br> New complaint has been lodged by " + user_data.name + "<br><br> Complaint Title : " + data.complaint_title + "<br> Complaint Description : " + data.complaint_description + "<br><br> Complainer Details : <br> Name : " + user_data.name + "<br> mail-id : " + user_data.email + "<br><br>Thank You :)";
                                            var msgBody_user = "Hello " + " " + user_data.email + ",<br><br> You have lodged a complaint in POLITICIAN-Application and has been recorded.<br> Your Complaint details are : Complaint Title : " + data.complaint_title + "<br> Complaint Description : " + data.complaint_description +
                                                "<br><br> Thank You :)";
                                            var mailid = config.secretory_mail_id
                                            let mailOptions = {
                                                from: '"POLITICIAN APPLICATION" <' + configuration.SENDER_EMAIL + '>', // sender address
                                                to: mailid,
                                                subject: 'Complaints - Politician - Application',
                                                text: "req.body.body",
                                                html: msgBody_secretory
                                            };
                                            email.sendMailNotification(mailOptions);
                                            let mailOptions_user = {
                                                from: '"POLITICIAN APPLICATION" <' + configuration.SENDER_EMAIL + '>', // sender address
                                                to: user_data.email,
                                                subject: 'Complaints - Politician - Application',
                                                text: "req.body.body",
                                                html: msgBody_user
                                            };
                                            email.sendMailNotification(mailOptions_user);
                                            callback({
                                                statuscode: 200,
                                                msg: "complaint added successfully",
                                                data: add_complaint
                                            });
                                        })
                                    }
                                })
                        } else {
                            callback({
                                statuscode: 404,
                                msg: "user_id not found"
                            });
                        }
                    } else {
                        callback({
                            statuscode: 304,
                            msg: "complaint_id exists"
                        });
                    }
                })
        }
        process.nextTick(complaint_data);
    },

    update_complaint_status: function (data, callback) {
        request_data = function () {
            if (data.complaint_id) {
                var status = data
                var mailid = config.secretory_mail_id;
                var db = app.db;
                const complaint = db.collection("complaints");
                let query = complaint.where('complaint_id', '==', status.complaint_id).get()
                    .then(snapshot => {
                        if (snapshot.empty) {
                            callback({
                                statuscode: 404,
                                msg: "complaint_id not found"
                            });
                        } else {
                            snapshot.forEach(doc => {
                                docdata = doc.data();
                                if (status.status_description !== "" && status.status_description !== undefined && status.status_description != null) {
                                    docdata.status_description = status.status_description;
                                } else if (status.status_description == undefined) {
                                    docdata.status_description = ""
                                }

                                if (status.complaint_status == 1) {
                                    docdata.complaint_status = 1
                                }
                                if (status.complaint_status == 2) {
                                    docdata.complaint_status = 2
                                }
                                if (status.complaint_status == 3) {
                                    docdata.complaint_status = 3
                                    //msgBody_secretory = "Hello " + " " + ",<br><br> You have accepted the complaint lodged by " + docdata.user_name + " is in processing state<br> Complaint Details <br> Complaint Title : " + docdata.complaint_title + "<br> Complaint Description : " + docdata.complaint_description + "<br><br> Thank You :)";
                                    //notification.body = "Complaint registered by you is in processing state";
                                }
                                if (status.complaint_status == 0) {
                                    docdata.complaint_status = 0
                                    //msgBody_secretory = "Hello " + " " + ",<br><br> You have opened the complaint lodged by " + docdata.user_name + " <br> Complaint Details <br> Complaint Title : " + docdata.complaint_title + "<br> Complaint Description : " + docdata.complaint_description + "<br><br> Thank You :)";
                                    //notification.body = "Complaint registered by you is in initial state or open state";
                                }

                                complaint.doc(doc.id).update(docdata);
                                const getuser = db.collection("users");
                                let query = getuser.where('user_id', '==', docdata.user_id).get()
                                    .then(snapshot => {
                                        if (snapshot.empty) {
                                            callback({
                                                statuscode: 404,
                                                msg: "Not able to find user"
                                            });
                                        } else {
                                            snapshot.forEach(doc => {
                                                user_data = doc.data();
                                                var message = {};
                                                var notification = {};
                                                if(status.complaint_status == 0){
                                                    notification.body = "Complaint registered by you is in initial state ";
                                                }
                                                if(status.complaint_status == 1){
                                                    notification.body = "Complaint registered by you is irrelevant";
                                                }
                                                if(status.complaint_status == 2){
                                                    notification.body = "Complaint registered by you is completed";
                                                }
                                                if(status.complaint_status == 3){
                                                    notification.body = "Complaint registered by you is inprogress";
                                                }
                                                message.to = user_data.fcmtoken;
                                                notification.title = "Complaint Update";
                                                //notification.body = "Complaint registered by you is updated";
                                                message.notification = notification;
                                                var data = {};
                                                data.category = "Complaint";
                                                data.notification_message = "Complaint registered by you is updated";
                                                message.data = data;
                                                fcm.send(message, function (err, messageId) {
                                                    if (messageId) {
                                                        console.log("message is sent");
                                                    }
                                                });
                                            });
                                        }
                                    });
                                callback({
                                    statuscode: 200,
                                    msg: "Complaint status changed successfully",
                                    data: docdata
                                });
                            });
                        }
                    });
            } else {
                callback({
                    statuscode: 404,
                    msg: "complaint_id not found"
                });
            }
        }
        process.nextTick(request_data);
    },

    get_complaint: function (data, callback) {
        complaint_data = function () {
            if (data.complaint_id) {
                var db = app.db;
                const getcomplaint = db.collection("complaints");
                let query = getcomplaint.where('complaint_id', '==', data.complaint_id).get()
                    .then(snapshot => {
                        if (snapshot.empty) {
                            callback({
                                statuscode: 404,
                                msg: "complaint_id not found"
                            });
                        } else {
                            snapshot.forEach(async doc => {
                                docdata = doc.data();
                                callback({
                                    statuscode: 200,
                                    msg: "Complaint details",
                                    data: docdata
                                });

                            });
                        }
                    })
            } else {
                callback({
                    statuscode: 404,
                    msg: "complaint_id not found"
                });
            }
        }
        process.nextTick(complaint_data);
    },

    list_complaints_user_id: function (data, callback) {
        complaint_data = function () {
            if (data.user_id) {
                var db = app.db;
                const getcomplaint = db.collection("complaints");
                var complaint_list = [];
                let query = getcomplaint.where('user_id', '==', data.user_id).get()
                    .then(snapshot => {
                        if (snapshot.empty) {
                            callback({
                                statuscode: 404,
                                msg: "user_id not found"
                            });
                        } else {
                            snapshot.forEach(async doc => {
                                docdata = doc.data();
                                await complaint_list.push(docdata);
                            });
                            callback({
                                statuscode: 200,
                                msg: "Complaint details",
                                data: complaint_list
                            });
                        }
                    })
            } else {
                callback({
                    statuscode: 404,
                    msg: "user_id not found"
                });
            }
        }
        process.nextTick(complaint_data);
    },

    list_complaints_complaint_status: function (data, callback) {
        complaint_data = function () {
            if (data.complaint_status) {
                var status = JSON.parse(data.complaint_status)
                var db = app.db;
                const getcomplaint = db.collection("complaints");
                var complaint_list = [];
                let query = getcomplaint.where('complaint_status', '==', status).get()
                    .then(snapshot => {
                        if (snapshot.empty) {
                            callback({
                                statuscode: 404,
                                msg: "complaint_status not found"
                            });
                        } else {
                            snapshot.forEach(async doc => {
                                docdata = doc.data();
                                await complaint_list.push(docdata);


                            });
                            callback({
                                statuscode: 200,
                                msg: "Complaint details",
                                data: complaint_list
                            });
                        }
                    })
            } else {
                callback({
                    statuscode: 405,
                    msg: "complaint_status not found"
                });
            }
        }
        process.nextTick(complaint_data);
    },


    complaints_filter: function (data, callback) {
        complaint_data = function () {
            var db = app.db;
            var complaintfilter = db.collection("complaints");
            var complaint_list = [];
            if (data.complaint_category) {
                complaintfilter = complaintfilter.where("complaint_category", "==", data.complaint_category);
            }
            if (data.taluk) {
                complaintfilter = complaintfilter.where("taluk", "==", data.taluk);
            }
            if (data.town_panchayat) {
                complaintfilter = complaintfilter.where("town_panchayat", "==", data.town_panchayat);
            }
            if (data.village_panchayat) {
                complaintfilter = complaintfilter.where("village_panchayat", "==", data.village_panchayat);

            }
            if (data.ward_no) {
                var num = JSON.parse(JSON.stringify(data.ward_no))
                complaintfilter = complaintfilter.where("ward_no", "==", data.ward_no);
            }
            if (data.complaint_status) {
                var status = JSON.parse(data.complaint_status)
                complaintfilter = complaintfilter.where("complaint_status", "==", status);
            }
            //  complaintfilter = complaintfilter.orderBy('created_at', 'desc').get()
            complaintfilter.get()
                .then(snapshot => {
                    if (snapshot.empty) {
                        callback({
                            statuscode: 404,
                            msg: "No complaints found"
                        });
                    } else {
                        snapshot.forEach(async doc => {
                            docdata = doc.data();
                            await complaint_list.push(docdata);
                        });
                        callback({
                            statuscode: 200,
                            msg: "Complaint details",
                            data: complaint_list
                        });
                    }
                })
        }
        process.nextTick(complaint_data);
    },

    list_complaints: function (callback) {
        ComplaintsData = function () {
            var db = app.db;
            const complaints_list = db.collection("complaints");
            let query = complaints_list.get()
                .then(snapshot => {
                    if (snapshot.empty) {
                        callback({
                            statuscode: 404,
                            msg: "Complaints not found"
                        });
                    } else {
                        var complaints_list = []
                        snapshot.forEach(async doc => {
                            docdata = doc.data();
                            complaints_list.push(docdata)

                        });
                        callback({
                            statuscode: 200,
                            msg: "Complaints List",
                            data: complaints_list
                        });
                    }
                })
        }
        process.nextTick(ComplaintsData);
    }


}
