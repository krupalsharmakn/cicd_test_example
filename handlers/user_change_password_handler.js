var bCrypt = require('bcrypt-nodejs');
var app = require('../app')
module.exports = {

    user_change_password: function (data, callback) {
        user_changepassword_data = function () {
            if(data.username){
            var db = app.db;
            const usersCollection = db.collection("users");

            let query = usersCollection.where('username', '==', data.username).get()
                .then(snapshot => {
                    if (snapshot.empty) {
                        callback({
                            "status": 404,
                            "msg": "User Not Found"
                        });

                    } else {
                        snapshot.forEach(doc => {
                            var docid = doc.id;
                            var docdata = doc.data();
                            var entered_password = data.password;
                            var encpassword = createHash(data.newpassword);
                            var encryptpassword = bCrypt.compareSync(entered_password.toString(), docdata.password);
                            if (encryptpassword == false) {
                                callback({
                                    statuscode: 404,
                                    msg: "Password not matching the username"
                                });

                            } else {
                                docdata.password = encpassword;
                                usersCollection.doc(doc.id).update(docdata);
                                callback({
                                    statuscode: 200,
                                    msg: "Password changed successfullt",
                                    data: docdata
                                });

                            }
                        });
                    }
                })
            }else{
                callback({
                    "statuscode": 404,
                    "msg": "User Not Found"
                });
            }
        };
        process.nextTick(user_changepassword_data);
    }
};
var createHash = function (password) {
    return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
};