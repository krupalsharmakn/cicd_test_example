var uuidV4 = require("../utils/utils");
var async = require('async');
var config = require("../config");
var app = require("../app");
var fs = require('fs');

module.exports = {
    add_department_contact: function (data, eventpath, callback) {
        departmentData = function () {
            var department_contact_id = uuidV4.getUniqueId();
            var db = app.db;
            const departmentcontact = db.collection("departmentcontact");
            let query = departmentcontact.where('department_contact_id', '==', department_contact_id).get()
                .then(async snapshot => {
                    var reports = "";
                    if (eventpath) {
                        let sampleFile = eventpath.image;
                        var date = new Date();
                        var d = date.getTime();
                        var fileExtension = sampleFile.name.substr((sampleFile.name.lastIndexOf('.') + 1));
                        var unique_id = uuidV4.getUniqueId();
                        var filename = d + unique_id + "." + fileExtension;
                        await sampleFile.mv(config.image_path + filename);
                        reports = config.get_image_path + filename;
                    } else {
                        reports = "";
                    }
                        if (snapshot.empty) {
                            let add_departmentcontact = {
                                "department_contact_id": department_contact_id,
                                "contact_person": data.contact_person,
                                "department": data.department,
                                "phone_number": data.phone_number,
                                "alternative_number": data.alternative_number,
                                "is_active": true,
                                "created_at": new Date(Date.now()).toISOString(),
                                "email": data.email,
                                "address": data.address,
                                "district": data.district,
                                "taluk": data.taluk,
                                "departmentcontact_images": reports,
                                "pin": data.pin,
                                "panchayath_ward": data.panchayath_ward,
                            }
                            let setDepartmnet = departmentcontact.doc().set(add_departmentcontact);
                            callback({
                                statuscode: 200,
                                msg: "Department contact added successfully",
                                data: add_departmentcontact
                            });
                        } else {
                            callback({
                                statuscode: 304,
                                msg: "department_contact_id  exists"
                            });
                        }
                   // }

                })
        }
        process.nextTick(departmentData);
    },

    edit_department_contact: function (data, eventpath, callback) {
        departmentcontact_data = function () {
            if (data.department_contact_id) {
                var db = app.db;
                const editdepartment = db.collection("departmentcontact");
                let query = editdepartment.where('department_contact_id', '==', data.department_contact_id).get()
                    .then(snapshot => {
                        if (snapshot.empty) {
                            callback({
                                statuscode: 404,
                                msg: "department_contact_id not found"

                            });
                        } else {
                            snapshot.forEach(async doc => {
                                docdata = doc.data();
                                var old_images = docdata.departmentcontact_images
                                if (eventpath) {
                                    let sampleFile = eventpath.image;
                                    var date = new Date();
                                    var d = date.getTime();
                                    var fileExtension = sampleFile.name.substr((sampleFile.name.lastIndexOf('.') + 1));
                                    var unique_id = uuidV4.getUniqueId();
                                    var filename = d + unique_id + "." + fileExtension;
                                    await sampleFile.mv(config.image_path + filename);
                                    reports = config.get_image_path + filename;
                                    docdata.departmentcontact_images = reports;
                                    // if (old_images != "" && old_images != null && old_images != undefined) {
                                    //     var oldimage = old_images.split('/');
                                    //     var last = oldimage;

                                    //     fs.unlink(config.image_path + last);
                                    // }

                                } else {
                                    docdata.departmentcontact_images = old_images;
                                }
                                if (data.contact_person !== undefined && data.contact_person !== "") {
                                    docdata.contact_person = data.contact_person;
                                }
                                if (data.department !== undefined && data.department !== "") {
                                    docdata.department = data.department;
                                }
                                if (data.phone_number !== undefined && data.phone_number !== "") {
                                    docdata.phone_number = data.phone_number;
                                }
                                if (data.alternative_number !== undefined && data.alternative_number !== "") {
                                    docdata.alternative_number = data.alternative_number;
                                }
                                if (data.email !== undefined && data.email !== "") {
                                    docdata.email = data.email;
                                }
                                if (data.district !== undefined && data.district !== "") {
                                    docdata.district = data.district;
                                }
                                if (data.taluk !== undefined && data.taluk !== "") {
                                    docdata.taluk = data.taluk;
                                }
                                if (data.pin !== undefined && data.pin !== "") {
                                    docdata.pin = data.pin;
                                }
                                if (data.address !== undefined && data.address !== "") {
                                    docdata.address = data.address;
                                }
                                if (data.panchayath_ward !== undefined && data.panchayath_ward !== "") {
                                    docdata.panchayath_ward = data.panchayath_ward;
                                }
                                editdepartment.doc(doc.id).update(docdata);
                                console.log("Updated")
                                callback({
                                    statuscode: 200,
                                    msg: "Department contact request updated successfully",
                                    data: docdata
                                });

                            });
                        }
                    })
            } else {
                callback({
                    statuscode: 404,
                    msg: "department_contact_id not found"
                });
            }
        }
        process.nextTick(departmentcontact_data);
    },

    get_department_contact: function (data, callback) {
        department_data = function () {
            if (data.department_contact_id) {
                var db = app.db;
                const getdepartment = db.collection("departmentcontact");
                let query = getdepartment.where('department_contact_id', '==', data.department_contact_id).get()
                    .then(snapshot => {
                        if (snapshot.empty) {
                            callback({
                                statuscode: 404,
                                msg: "department_contact_id not found"
                            });
                        } else {
                            snapshot.forEach(async doc => {
                                docdata = doc.data();
                                callback({
                                    statuscode: 200,
                                    msg: "Department contact details updated successfully",
                                    data: docdata
                                });
                            });
                        }
                    })
            } else {
                callback({
                    statuscode: 404,
                    msg: "department_contact_id not found"
                });
            }
        }
        process.nextTick(department_data);
    },

    deactivate_department_contact: function (data, callback) {
        department_data = function () {
            if (data.department_contact_id) {
                var db = app.db;
                const deactivatedepartment = db.collection("departmentcontact");
                deactivatedepartment.where('department_contact_id', '==', data.department_contact_id).get()
                    .then(snapshot => {
                        if (snapshot.empty) {
                            callback({
                                statuscode: 404,
                                msg: "department_contact_id not found"
                            });
                        } else {
                            snapshot.forEach(doc => {
                                docdata = doc.data();
                                if (data.is_active == true) {
                                    docdata.is_active = true
                                }
                                if (data.is_active == false) {
                                    docdata.is_active = false
                                }
                                deactivatedepartment.doc(doc.id).update(docdata);
                                console.log("Updated")
                                callback({
                                    statuscode: 200,
                                    msg: " Department contact deactivated successfully",
                                    data: docdata
                                });
                            });
                        }
                    })
            } else {
                callback({
                    statuscode: 404,
                    msg: "department_contact_id not found"
                });
            }
        };
        process.nextTick(department_data);
    },

    list_departmentcontact_by_department: function (data, callback) {
        department_data = function () {
            // if (data.department) {
            var db = app.db;
            var getdepartment = db.collection("departmentcontact");
            var department_list = []
            if (data.department) {
                getdepartment = getdepartment.where("department", "==", data.department);
            }
            if (data.taluk) {
                getdepartment = getdepartment.where("taluk", "==", data.taluk);
            }
            if (data.district) {
                getdepartment = getdepartment.where("district", "==", data.district);
            }
            if (data.panchayath_ward) {
                getdepartment = getdepartment.where("panchayath_ward", "==", data.panchayath_ward);
            }
            if (data.pin) {
                getdepartment = getdepartment.where("pin", "==", data.pin);
            }
            getdepartment.get()
                .then(snapshot => {
                    if (snapshot.empty) {
                        callback({
                            statuscode: 404,
                            msg: "No Department contacts for this department"
                        });
                    } else {
                        snapshot.forEach(async doc => {
                            docdata = doc.data();
                            department_list.push(docdata)

                        });
                        callback({
                            statuscode: 200,
                            msg: "Department contact List",
                            data: department_list
                        });
                    }
                })
            // } else {
            //     callback({
            //         statuscode: 404,
            //         msg: "Department not found"
            //     });
            // }
        }
        process.nextTick(department_data);
    },

    add_department_type: function (data, callback) {
        departmentData = function () {
            var department_type_id = uuidV4.getUniqueId();
            var db = app.db;
            const departmenttype = db.collection("departmenttype");
            let query = departmenttype.where('department_type_id', '==', department_type_id).get()
                .then(async snapshot => {
                    if (snapshot.empty) {
                        let add_departmenttype = {
                            "department_type_id": department_type_id,
                            "department_type": data.department_type,
                            "is_active": true,
                            "created_at": new Date(Date.now()).toISOString()
                        }
                        let setDepartmnet = departmenttype.doc().set(add_departmenttype);
                        callback({
                            statuscode: 200,
                            msg: "Department type added successfully",
                            data: add_departmenttype
                        });
                    } else {
                        callback({
                            statuscode: 304,
                            msg: "department_type_id  exists"
                        });
                    }
                })
        }
        process.nextTick(departmentData);
    },

    list_departmentcontact: function (data, callback) {
        department_data = function () {
            if (data.is_active) {
                var status = JSON.parse(data.is_active)
                var db = app.db;
                const listdepartmentcontact = db.collection("departmentcontact");
                var departmentcontact_list = [];
                let query = listdepartmentcontact.where('is_active', '==', status).get()
                    .then(snapshot => {
                        if (snapshot.empty) {
                            callback({
                                statuscode: 404,
                                msg: "departmentcontact_status not found"
                            });
                        } else {
                            snapshot.forEach(async doc => {
                                docdata = doc.data();
                                await departmentcontact_list.push(docdata);


                            });
                            callback({
                                statuscode: 200,
                                msg: "departmentcontact details",
                                data: departmentcontact_list
                            });
                        }
                    })
            } else {
                callback({
                    statuscode: 405,
                    msg: "departmentcontact_status not found"
                });
            }
        }
        process.nextTick(department_data);
    },

    list_all_departmentcontact: function (callback) {
        department_data = function () {
            var db = app.db;
            const listdepartmentcontact = db.collection("departmentcontact");
            var departmentcontact_list = [];
            let query = listdepartmentcontact.get()
                .then(snapshot => {
                    if (snapshot.empty) {
                        callback({
                            statuscode: 404,
                            msg: "departmentcontact not found"
                        });
                    } else {
                        snapshot.forEach(async doc => {
                            docdata = doc.data();
                            await departmentcontact_list.push(docdata);


                        });
                        callback({
                            statuscode: 200,
                            msg: "departmentcontact list",
                            data: departmentcontact_list
                        });
                    }
                })
        }
        process.nextTick(department_data);
    }
}
