var mongoose = require('mongoose');
module.exports = mongoose.model('user', {
    user_id: {type: String, default: 0},
    portal_type: {type: String, default: 0},
    user_role: {type: String, default: 0},
    official_email: {type: String, default: 0},
    alternate_email: {type: String, default: 0},
    first_name: {type: String, default: 0},
    first_login: {type: Boolean, default: true},
    last_name: {type: String, default: 0},
    username: {type: String, default: 0},
    password: {type: String, default: 0},
    isActive: {type: Boolean, default: true},
    email_verified: {type: Boolean, default: true},
    phone: {type: String, default: 0},
    passtoken: {type: String, default: 0},
    resetpasswordtoken: {type: String, default: 0},
    profile_image: {type: String, default: 0},
    signup_type: {type: String, default: 0},
    dob: Date,
    createdAt: {type: Date, default: Date.now},
    domain_name: String,
    updatedAt: Date

});